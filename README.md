sudo apt-get install postgresql

sudo pip install pyscopg2

sudo -i -u postgres

In centos:
cd /home/
mkdir installers
cd installers
 
wget https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-6-x86_64/pgdg-centos96-9.6-3.noarch.rpm

rpm -i pgdg-centos96-9.6-3.noarch.rpm

yum install postgresql96 postgresql96-contrib postgresql96-server postgresql96-devel postgresql96-plpython postgis2_96 postgis2_96-devel postgis2_96-client postgis2_96-utils

service postgresql-9.6 initdb 'en_US.UTF8'

# or

# service postgresql-9.6 initdb 'es_ES.UTF-8'

 
chkconfig postgresql-9.6 on replace ident or peer with md5 or trust in "/var/lib/pgsql/9.6/data/pg_hba.conf"

add "local    postgres     postgres     peer" in the begining of "/var/lib/pgsql/9.6/data/pg_hba.conf"

postgres=# \du
List of roles

 Role name |                         Attributes                         | Member of 
 -----------+------------------------------------------------------------+-----------
 postgres  | Superuser, Create role, Create DB, Replication, Bypass RLS | {}
 probe     |                                                            | {}
  

postgres=# ALTER USER probe WITH SUPERUSER;
ALTER ROLE
postgres=# ALTER USER probe WITH CREATEDB;
ALTER ROLE
postgres=# ALTER USER probe WITH CREATEROLE;
ALTER ROLE


postgres@SomeHostName:$ createuser probe -P --interactive

postgres@SomeHostName:$ createdb s1mmedb -U probe -h localhost

postgres@SomeHostName:$ [dropuser probe]

postgres@SomeHostName:$ [dropdb s1mmedb]

postgres@SomeHostName:~$ psql s1mmedb -U probe -h localhost
Password for user probe:
psql (9.3.20)
SSL connection (cipher: DHE-RSA-AES256-GCM-SHA384, bits: 256)
Type "help" for help.


s1mme=> postgres@Ganesha:~$ psql s1mmedb -U probe -h localhost
Password for user probe:
psql (9.3.20)
SSL connection (cipher: DHE-RSA-AES256-GCM-SHA384, bits: 256)
Type "help" for help.

s1mmedb=> GRANT ALL ON DATABASE s1mmedb TO probe;
s1mmedb=> \d [tdun]
s1mmedb=> \l
s1mmedb=> \q
s1mmedb=> \h
s1mmedb=> \d+ <table_name>



postgres@SomeHostName:$ psql s1mmedb -U probe -h localhost

s1mmedb=>GRANT ALL ON DATABASE s1mmedb TO probe;


# Way to generate pb2.py file from proto file
protoc -I=/home/anagaraj/PycharmProjects/TestPyShark/traffic_analyer/proto --python_out=/home/anagaraj/PycharmProjects/TestPyShark/traffic_analyser/proto /home/anagaraj/PycharmProjects/TestPyShark/traffic_analyser/proto/Communication.proto


# Way to terminate all connection to db. i.e to force terminate all connection to db
SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = 's1mmedb';

# Adding more connection to DB
sudo vi /etc/postgresql/9.3/main/postgresql.conf
sudo service postgresql restart
