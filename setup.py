#!/usr/bin/env python
"""Traffic Analyser"""

from setuptools import setup, find_packages
from codecs import open
from os import path
import platform


here = path.abspath(path.dirname(__file__))
target_conf_dir = '/etc/cellos/probe/'
loginit_file = 'data/traffic_analyser.loginit'
conf_file = 'data/traffic_analyser.conf'
os_platform = ""
# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


def main():
    """Main Function."""
    setup(
        name="traffic_analyser",
        version="0.1",
        description="Call Trace Tool",
        long_description=long_description,
        author="CellOS DEP Team",
        url="https://www.cellossoftware.com",
        packages=find_packages(),
        classifiers=[
            "Development Status :: Demo",
            "Intended Audience :: Developers",
            'Programming Language :: Python :: 2.7',
        ],
        python_requires=">=2.7, <3",
        install_requires=[
            "argparse",
            "pyshark",
            "psycopg2",
            "blockdiag",
            "seqdiag",
            "protobuf==2.6.1",
        ],
        data_files=[
            (target_conf_dir, [loginit_file, conf_file])
        ],
        entry_points={
            'console_scripts': [
                'traffic_analyser = traffic_analyser.TestPyShark:main'
            ]
        }

    )

if __name__ == '__main__':
    main()
