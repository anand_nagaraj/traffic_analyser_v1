#!/usr/bin/env python

import time
import ConfigParser
from logging.config import fileConfig
import argparse
import os

from conf.ConfigData import ConfigData
from TrafficAnalyser import *
from multiprocessing import Lock
# from Layers.Counters import MainCollector
from Collector import CollectorWorkerClass

processors = ['S1AP', 'DIAMETER', 'DATA']


def WaitForAllWorkers_ToFinish(workers):
    logger.info("Waiting for all TA/CF Workers to finish.")
    for worker in workers:
        worker.join()

    logger.info("Finshed all TA/CF Workers.")

def WaitForCollector(mainCollector, collectorWorker, collectorCarrier):
    tup = ("EOF", None, None)
    serialized_data = mainCollector.pickle_pack(tup)
    collectorCarrier.put(serialized_data)
    collectorWorker.join()

def PrepareDBConnectionForCleanup(configData):
    sqlConnection, dblock = PrepareDBConnection(configData)

    sqlConnection.CreateAllElements()
    sqlConnection.Commit()



def PrepareDBConnection(configData):
    dblock = Lock()
    sqlConnection = ConnectToDB(configData, lock=dblock)
    if sqlConnection is None:
        ret = 'Failure in connecting to Database. Check the connection parameters'
        logger.error(ret)
        raise ret

    # FIXME: Should we drop here ?
    sqlConnection.DropViews()
    sqlConnection.DropTables()
    sqlConnection.Commit()

    return sqlConnection, dblock

def PrepareDBConnectionForViews(configData):
    sqlConnection = ConnectToDB(configData)
    if sqlConnection is None:
        ret = 'Failure in connecting to Database. Check the connection parameters'
        logger.error(ret)
        raise ret

    return sqlConnection

def PrintTimeElapsed(settingTime, parsingTime, seqPlotTime, path):
    logger.info("Settingup time took %s" \
                % (human_readable(settingTime)))
    logger.info("Running files in \'%s\' took %s" \
                % (path, human_readable(parsingTime)))
    logger.info("Generating call flow graph took %s" \
                % (human_readable(seqPlotTime)))
    logger.info("Total completion time = %s" \
                % (human_readable(settingTime + parsingTime + seqPlotTime)))


def DistributePcapFilesToWorkers(pcap_path, ta_workers):
    fnames = []
    ta_pcap_fnames = []

    if not os.path.isdir(pcap_path):
        print("Pcap Path does not exist")
        exit(0)

    for pack in os.walk(pcap_path):
        for files in pack[2]:
            fnames += [files]

    if not len(fnames):
        logger.error("No files could be fetched")
        exit(0)

    for ind in range(ta_workers):
        ta_pcap_fnames.append([])

    for ind in range(len(fnames)):
        ta_pcap_fnames[ind % ta_workers].append(fnames[ind])

    logger.info(ta_pcap_fnames)

    return ta_pcap_fnames


def InitiateTrafficAnalyser(startTime, sqlConnection, configData, ta_pcap_fnames,
                            dblock, mainCollector, collectorCarrier):
    ta_workers = []
    logger.info(configData)

    for ind in range(len(ta_pcap_fnames)):
        configData.pcap_files = ta_pcap_fnames[ind]
        ta = TrafficAnalyser(startTime, sqlConnection, configData, mainCollector, collectorCarrier)
        worker = ta.PrepareTAWorker(ind + 1, dblock)
        ta_workers.append(worker)

    WaitForAllWorkers_ToFinish(ta_workers)


def GenerateCallGraph(sqlConnection, seqDiaPath, src_ppath, dst_ppath, filt, imsi_list,
                      msisdn_list, mmeiplist, imsi_png_pop, imsi_pcap_pop, pcapwriters):
    seqConfig = SeqDiagramConfig(sqlConnection, seqDiaPath, src_ppath,
                                 dst_ppath, filt, imsi_png_pop, imsi_pcap_pop,
                                 pcapwriters)

    if len(imsi_list[0]):
        for imsi in imsi_list:
            msisdn = None
            sqlConnection.CreateAllViews(imsi, msisdn)
            seqPlotter = SeqDiagramPlotter(seqConfig)
            cf_workers = seqPlotter.initialize_workers()
            seqPlotter.GenerateCallGraphViaViews(msisdn, mmeiplist)
    if len(msisdn_list[0]):
        for msisdn in msisdn_list:
            imsi = None
            sqlConnection.CreateAllViews(imsi, msisdn)
            seqPlotter = SeqDiagramPlotter(seqConfig)
            cf_workers = seqPlotter.initialize_workers()
            seqPlotter.GenerateCallGraphViaViews(msisdn, mmeiplist)
    if not len(imsi_list[0]) and not len(msisdn_list[0]):
        imsi = None
        msisdn = None
        sqlConnection.CreateAllViews(imsi, msisdn)
        seqPlotter = SeqDiagramPlotter(seqConfig)
        cf_workers = seqPlotter.initialize_workers()
        seqPlotter.GenerateCallGraphViaViews(msisdn, mmeiplist)

    # Wait for all CF workers to finish.
    WaitForAllWorkers_ToFinish(cf_workers)


def _parser_():
    """Parse command line options."""
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--conf",
                        default="/etc/cellos/probe/traffic_analyser.conf",
                        help="Provide Trafic Analyser Configuration Path"
                        )
    parser.add_argument("-l", "--loginit",
                        default="/etc/cellos/probe/traffic_analyser.loginit",
                        help="Provide Loginit Configuration Path"
                        )
    return parser.parse_args()



def main():
    startTime = time.time()
    args = _parser_()
    if not os.path.exists(args.loginit):
        print("%s does not exist" % args.loginit)
        return
    if not os.path.exists(args.conf):
        print("%s does not exist" % args.conf)
        return

    fileConfig(args.loginit)
    logger = logging.getLogger()
    logger.info(args.loginit)
    logger.info(args.conf)

    try:
        config = ConfigParser.ConfigParser()
        config.read(args.conf)
        pcap_path = config.get('config', 'pcap_path')
        seq_dia_path = config.get('config', 'seq_dia_path')
        message_type = config.get('config', 'message_type')
        package_type = config.get('config', 'package_type')
        mme_ip = config.get('config', 'mme_ip').split(",")
        ta_workers = int(config.get('config', 'ta_workers'))
        cf_workers = int(config.get('config', 'cf_workers'))

        dbhost = config.get('database', 'host')
        postgres_dbuser = config.get('database', 'ps_user')
        postgres_dbpasswd = config.get('database', 'ps_passwd')

        probe_dbname = config.get('database', 'probe_db')
        probe_dbuser = config.get('database', 'probe_user')
        probe_dbpasswd = config.get('database', 'probe_passwd')


        db_commit_time = config.get('database', 'commit_time')
        dst_pcap_path = config.get('pcapgeneration', 'dst_pcap_path')
        jump_ta = config.get('misc', 'jump_ta')
        only_create_db = config.get('misc', 'only_create_db')
        filt = config.get('misc', 'filter')

        imsi = config.get('SeqDiagram', 'IMSI')
        msisdn = config.get('SeqDiagram', 'MSISDN')
        imsi_png_pop = config.get('SeqDiagram', 'IMSI_PNG_POP').split(',')
        imsi_pcap_pop = config.get('SeqDiagram', 'IMSI_PCAP_POP').split(',')


        configData = ConfigData(
            pcap_path,
            seq_dia_path,
            message_type,
            package_type,
            mme_ip,
            probe_dbname,
            probe_dbuser,
            probe_dbpasswd,
            dbhost,
            postgres_dbuser,
            postgres_dbpasswd,
            imsi,
            msisdn,
            imsi_png_pop,
            imsi_pcap_pop,
            db_commit_time
        )

        imsi_list = imsi.split(",")
        msisdn_list = msisdn.split(",")

        if not len(mme_ip) or mme_ip[0] == '':
            raise Exception("MMEIP configuration is Empty... For V1 release, please provide valid IP's")

    except Exception as e:
        logger.error('Exception was raised due to \'%s\' ' % e)
        exit(1)

    try :
        if int(only_create_db):
            PrepareDBConnectionForCleanup(configData)
            return;

        # This is only used for testing seqDiagram and dumping pcap files quickly.
        if int(jump_ta):
            sqlConnection = PrepareDBConnectionForViews(configData)
            # Generate the Call Graph
            startTime = time.time()
            GenerateCallGraph(sqlConnection, configData.seqDiaPath,
                              pcap_path, dst_pcap_path, filt, imsi_list,
                              msisdn_list, mme_ip, imsi_png_pop, imsi_pcap_pop,
                              cf_workers)
            seqPlotTime = time.time() - startTime
            PrintTimeElapsed(0, 0, seqPlotTime, configData.pcap_path)
            sqlConnection.Close()
            return


        cntlock = Lock()
        mainCollector = CollectorWorkerClass(cntlock)
        collectorWorker, collectorCarrier = CollectorWorkerClass(cntlock).PrepareCollectorWorker()

        ta_pcap_fnames = DistributePcapFilesToWorkers(pcap_path, ta_workers)

        sqlConnection, dblock = PrepareDBConnection(configData)

        settingTime = time.time() - startTime

        # Start Traffic Analyser
        startTime = time.time()
        InitiateTrafficAnalyser(startTime, sqlConnection, configData,
                                ta_pcap_fnames, dblock, mainCollector, collectorCarrier)
        WaitForCollector(mainCollector, collectorWorker, collectorCarrier)
        parsingTime = time.time() - startTime

        # Generate the Call Graph
        startTime = time.time()
        GenerateCallGraph(sqlConnection, configData.seqDiaPath,
                          pcap_path, dst_pcap_path, filt, imsi_list,
                          msisdn_list, mme_ip, imsi_png_pop, imsi_pcap_pop,
                          cf_workers)
        seqPlotTime = time.time() - startTime


        # Print time spent the most
        PrintTimeElapsed(settingTime, parsingTime, seqPlotTime, configData.pcap_path)

        sqlConnection.Close()

    except Exception as e:
        logger.exception("Exception raised: %s" %e)
        exit(1)

    # # FIXME: Remove this after the test. This section should only be used for seqDiagram testing.
    # sqlConnection = ConnectToDB(configData)
    # seqPlotter = SeqDiagramPlotter(sqlConnection, "/tmp/Diagram/Sorted2mins_Dia_")
    # seqPlotter.GenerateCallGraphViaViews()
    # return
    # # End tesing seqDiagram here.


if __name__ == "__main__":
    main()
