class ConfigData():
    def __init__(self, pcap_path, seqDiaPath, ipc_type,
                 pkg_type, mmeip, probe_db, probe_user, probe_passwd,
                 dbhost, postgres_dbuser, postgres_dbpasswd,imsi, msisdn,
                 imsi_png_pop, imsi_pcap_pop,
                 db_commit_time = "60"):
        self.pcap_path = pcap_path
        self.pcap_files = []
        self.seqDiaPath = seqDiaPath
        self.message_type = ipc_type
        self.pkg_type = pkg_type
        self.mme_ip = mmeip
        self.probe_db = probe_db
        self.probe_user = probe_user
        self.probe_passwd = probe_passwd
        self.dbhost = dbhost
        self.db_commit_time = int(db_commit_time)
        self.ps_user = postgres_dbuser
        self.ps_passwd = postgres_dbpasswd
        self.imsi = imsi
        self.msisdn = msisdn
        self.imsi_png_pop = imsi_png_pop
        self.imsi_pcap_pop = imsi_pcap_pop

    def __str__(self):
        return "Config as read from configuration file\n" \
               "[config]\n" \
               " pcap_path = %s\n" \
               " pcap_files = %s\n" \
               " seq_dia_path = %s\n" \
               " message_type = %s\n" \
               " package_type = %s\n" \
               " mme_ip = %s\n" \
               "\n[database]\n" \
               " probe_db = %s\n" \
               " probe_user = %s\n" \
               " probe_passwd = %s\n" \
               " host = %s\n" \
               " ps_user = %s\n" \
               " ps_passwd = %s\n" \
               " commit time = %d\n" \
               "\n[SeqDiagram]\n" \
               " IMSI = %s\n" \
               " MSISDN = %s\n" \
               " IMSI_PNG_POP = %s\n" \
               " IMSI_PCAP_POP = %s\n"\
               % (self.pcap_path, self.pcap_files, self.seqDiaPath, self.message_type, self.pkg_type, self.mme_ip,
                  self.probe_db, self.probe_user, self.probe_passwd, self.dbhost,
                  self.ps_user, self.ps_passwd, self.db_commit_time, self.imsi, self.msisdn,
                  self.imsi_png_pop, self.imsi_pcap_pop)
