"""
MultiProcessing.py
"""
import logging
import pickle
import proto.Communication_pb2

from DataBase.PostGreSQLDB import *
from DataBase.SequenceDiagram import *

from Layers.DiameterLayer import *
from Layers.SCTPLayer import *
from Layers.GTPLayer import *
from multiprocessing import Process, Pipe, Queue
import string

logger = logging.getLogger('')


def modify_dict_values(layer):
    for key,value in layer.items():
        sts = value.replace('[', '').split(']')
        value = [s.split(',') for s in sts]
        lst1=[]

        for i in range(len(value)):
            lst2=[]
            for j in range(len(value[i])):
                rslt = value[i][j].translate(string.maketrans("", ""), "'(): ")
                if len(rslt) is not 0:
                    lst2.append(rslt)
            if len(lst2) is not 0:
                lst1.append(lst2)

        layer[key] = lst1

    return layer



class CommonData():
    def __init__(self, type, pkt_count=None, pkt_time=None, srcip=None, dstip=None,
                 layer=None, fieldNames=None, req=0):
        self.type = type
        self.pkt_count = pkt_count
        self.pkt_time = pkt_time
        self.srcip = srcip
        self.dstip = dstip
        self.layer = layer
        self.fieldNames = fieldNames
        self.req = req
        pickle.HIGHEST_PROTOCOL


    def reset(self):
        self.type = ''
        self.pkt_count = 0
        self.pkt_time = 0
        self.srcip = ''
        self.dstip = ''
        self.layer = {}
        self.fieldNames = []
        self.req = 0

    def get(self):
        return [self.type, self.pkt_count, self.pkt_time, self.srcip, self.dstip,
                self.layer, self.fieldNames, self.req]

    def pickle_pack(self):
        return pickle.dumps(self.get())

    def pickle_unpack(self, msg):
        return pickle.loads(msg)

    def google_pack(self, layer_type):
        channelData = proto.Communication_pb2.ChannelData()
        channelData.type = self.type
        if self.pkt_count is not None:
            channelData.pkt_count = self.pkt_count
            channelData.pkt_time = self.pkt_time
            channelData.srcip = self.srcip
            channelData.dstip = self.dstip
            if len(self.layer): # This is a dictionary
                layer = channelData.layer
                for key, value in self.layer.items():
                    field = layer.fields.add()
                    field.key = key
                    if layer_type == "DATA":
                        field.value = ' '.join(str(x) for x in value)
                    else:
                        field.value = str(value[0])
                if len(self.fieldNames):
                    for fname in self.fieldNames:
                        name = layer.names.add()
                        name.name = fname

                layer.req = int(self.req)

        return channelData.SerializeToString()



    def google_unpack(self, msg):
        try:
            self.reset()
            channelData = proto.Communication_pb2.ChannelData()
            channelData.ParseFromString(msg)
            self.type = channelData.type
            if channelData.type == 'data':
                self.pkt_count = channelData.pkt_count if channelData.HasField('pkt_count') else None
                self.pkt_time = channelData.pkt_time if channelData.HasField('pkt_time') else None
                self.srcip = channelData.srcip if channelData.HasField('srcip') else None
                self.dstip = channelData.dstip if channelData.HasField('dstip') else None

                if channelData.HasField('layer'):
                    layer = channelData.layer
                    for field in layer.fields:
                        if field.key is not None:
                            k = field.key
                            v = field.value
                            self.layer[k] = str(v)
                    for name in layer.names:
                        if name is not None:
                            self.fieldNames.append(name.name)

                    self.req = layer.req

            return self.get()

        except IOError:
            logger.info('Unable to Unserialize to GPB')
        except Exception as e:
            print "Incorrect %s for %s pkt=%s" % (e, self.type, self.pkt_count)


class MultiProcessor:
    def __init__(self, procs, configData):
        self.stop = False
        self.map_queues = {} # Either queues or pipes should be used, but not both.
        self.map_pipes = {}  # Pipes will perform better than queues.
        self.map_workers = {}
        self.processors = {}
        self.procs = procs
        self.map_proc_pipe_workers = {} # key is 'process' value is tuple of [worker, pipe]
                                        #  where pipe is tuple of (output_p, input_p)

        self.map_sqlOperation = {}
        self.google = True if configData.pkg_type == 'gpb' else False
        self.sqlConnection = None

    def CreateQueues(self):
        for process in self.procs:
            que = Queue()
            if que is not None:
                self.map_queues[process] = que

    def CreatePipes(self):
        for process in self.procs:
            output_p, input_p = Pipe()
            if output_p is not None and input_p is not None:
                self.map_pipes[process] = (output_p, input_p)


    def StartWorkerProcess(self, process, sqlOperation, carrier, configData, dblock,
                           mainCollector, collectorCarrier, fname):
        logger.info('Created process %s with carrier %s' % (process, carrier))
        if self.sqlConnection is None:
            self.sqlConnection = ConnectToDB(configData, process, dblock)
            if self.sqlConnection is not None:
                self.sqlConnection.CreateAllElements()
            else :
                logger.error('Database is not set, cannot start any worker')
                return


        if process == 'S1AP':
            sqlS1 = SQLS1Operation(self.sqlConnection)
            s1apProcessor = S1APProcessor(sqlS1, mainCollector, collectorCarrier,
                                          configData.mme_ip)
            self.processors['S1AP'] = s1apProcessor
            sqlOperation['S1AP'] = sqlS1
        elif process == 'DIAMETER':
            sqlS6 = SQLS6Operation(self.sqlConnection)
            s6Processor = S6Processor(sqlS6, mainCollector, collectorCarrier,
                                      configData.mme_ip)
            self.processors['DIAMETER'] = s6Processor
            sqlOperation['DIAMETER'] = sqlS6
        elif process == 'DATA':
            sqlgtpv2 = SQLGTPCOperation(self.sqlConnection)
            gtpv2Processor = GTPV2Processor(sqlgtpv2, mainCollector, collectorCarrier,
                                            configData.mme_ip)
            self.processors['DATA'] = gtpv2Processor
            sqlOperation['DATA'] = sqlgtpv2

        msg = ''
        if self.map_pipes.has_key(process):
            commonData = CommonData('')
            output_p, input_p = carrier
            input_p.close()  # We are only reading
            while True:
                try:
                    msg = output_p.recv()  # Read from the output pipe and do nothing

                    cd_inlist = commonData.google_unpack(msg) if self.google else commonData.pickle_unpack(msg)
                    proto_type, pkt_count, pkt_time, srcip, dstip, layer, fieldnames, req = cd_inlist
                    if proto_type == 'data':
                        if process == 'S1AP':
                            self.processors['S1AP'].process_s1ap_layer(pkt_count, pkt_time,
                                                                       srcip, dstip, layer, fieldnames,
                                                                       fname)
                        elif process == 'DIAMETER':
                            self.processors['DIAMETER'].parse_diameter_layer(pkt_count, pkt_time,
                                                                             srcip, dstip, layer, req,
                                                                             fname)
                        elif process == 'DATA':
                            layer = modify_dict_values(layer)
                            self.processors['DATA'].process_gtpv2_layer(pkt_count, pkt_time,
                                                                        srcip, dstip, layer,
                                                                        fname)

                    elif proto_type == 'dbCommit':
                        self.CommitToDB()
                    elif proto_type == 'final':
                        self.finalRecordCount()
                    elif proto_type == 'dbClose':
                        self.CloseAllSQLConnection()
                    elif proto_type == 'getCounters':
                        self.processors[process].push_counters()
                    elif proto_type == 'EOF':
                        raise EOFError

                except EOFError:
                    logger.info("Received last messages so far on %s" % process)
                    break
        else:
            logger.info('Created process %s with carrier as queue (expensive) %s' % (process, carrier))
            logger.info('FIXME: Work on implementation on queue.')



    def PushPacketToWorker(self, packet, pkt_count):
        srcip, dstip = extract_src_dst_ip_address(packet)
        pkt_time = get_pkt_time(packet)

        for layer in packet.layers:
            name = layer.layer_name.upper()

            if self.map_pipes.has_key(name):
                if name == 'DATA':
                    layerFields = extractAllFields(layer, True)
                else:
                    layerFields = extractAllFields(layer)
                carrier = self.map_pipes[name]
                req = layer.flags_request[0] if name == 'DIAMETER' else 0
                cd = CommonData('data', pkt_count, pkt_time, srcip, dstip, layerFields, layer.field_names, req)

                msg_tup = cd.google_pack(name) if self.google else cd.pickle_pack()  # Serialize the data here

                output_p, input_p = carrier
                input_p.send(msg_tup)

            elif self.map_queues.has_key(name):
                layerFields = extractAllFields(layer)
                req = layer.flags_request[0] if name == 'DIAMETER' else 0
                carrier = self.map_queues[name]
                cd = CommonData('data', pkt_count, pkt_time, srcip, dstip, layerFields, layer.field_names, req)

                msg_tup = cd.google_pack(name) if self.google else cd.pickle_pack()  # Serialize the data here

                carrier.put(msg_tup)

    def PushWorkerToAdditionalWork(self, proto_type):
        for name in self.map_workers.keys():
            if self.map_pipes.has_key(name):
                carrier = self.map_pipes[name]
                cd = CommonData(proto_type)

                msg_tup = cd.google_pack(proto_type) if self.google else cd.pickle_pack()  # Serialize the data here

                output_p, input_p = carrier
                input_p.send(msg_tup)

    def CreateAllReceiverWorkers(self, configData, dblock, mainCollector,
                                 collectorCarrier, index, fname):
        if len(self.map_queues) == 0 and len(self.map_pipes) == 0:
            return 1

        if len(self.map_queues) and len(self.map_pipes):
            return 2


        for process in self.procs:
            if self.map_pipes.has_key(process):
                carrier = self.map_pipes[process] # Always have pipe here.
            elif self.map_queues.has_key(process):
                carrier = self.map_queues[process]  # Using queue here

            receiver = Process(name=process + '_Proc_' + str(index), target=self.StartWorkerProcess,
                               args=(process, self.map_sqlOperation, carrier, configData,
                                     dblock, mainCollector, collectorCarrier, fname))

            self.map_workers[process] = receiver

        return 0


    def StartAllWorkers(self):
        for key, worker in self.map_workers.items():
            logger.info("%s : %s" %(key, worker))
            worker.start()

            if self.map_pipes.has_key(key):
                output_p, input_p = self.map_pipes[key]
                output_p.close() # We no longer need this part of the pipe()

        logger.info('Started all workers here.....')


    def WaitUntilAllWorkersFinished(self):
        allKeys = list(self.map_pipes.keys())
        for key in allKeys:
            if self.map_workers.has_key(key):
                worker = self.map_workers[key]
                logger.info("Stopping workers %s : %s" % (key, worker))
                worker.join()


    def CloseAllInputDescriptor(self):
        allKeys = list(self.map_pipes.keys())
        for key in reversed(allKeys):
            if self.map_pipes.has_key(key):
                pipe = self.map_pipes[key]
                output_p, input_p = pipe
                input_p.close()
                output_p.close()
                logger.info("Stopping carriers %s : %s" % (key, pipe))


    def CommitToDB(self):
        for sqlOp in self.map_sqlOperation.values():
            sqlOp.getSqlConnection().Commit()

    def finalRecordCount(self):
        for sqlOp in self.map_sqlOperation.values():
            sqlOp.finalRecordCount()


    def CloseAllSQLConnection(self):
        for sqlOp in self.map_sqlOperation.values():
            sqlOp.getSqlConnection().Close()
