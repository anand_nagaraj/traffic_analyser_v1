import psycopg2
import logging
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

logger = logging.getLogger('')

def ConnectToDB(configData, process=None, lock=None):
    dbname = "dbname=\'" + configData.probe_db + "\'"
    host = " host=\'" + configData.dbhost + "\'"
    user = " user=\'" + configData.probe_user + "\'"
    passwd = " password=\'" + configData.probe_passwd + "\'"

    fullStringConn = dbname + host + user + passwd
    logger.info('Connecting to DB using ' + fullStringConn)
    create_user_db(configData)
    sqlConnection = DBConnection(fullStringConn, process, lock)

    return sqlConnection


def create_user_db(configData):
    try:
        default_dbname = "dbname=\'" + "postgres" + "\'"
        default_host = " host=\'" + configData.dbhost + "\'"
        default_user = " user=\'" + configData.ps_user + "\'"
        default_passwd = " password=\'" + configData.ps_passwd + "\'"

        default_conn_string = default_dbname + default_host + default_user + default_passwd
        logger.info(default_conn_string)

        condb = psycopg2.connect(default_conn_string)
        condb.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
        curs = condb.cursor()
        curs.execute("select 1 from pg_roles where rolname = \'" + configData.probe_user + "\'")
        if curs.rowcount == 0:
            curs.execute("CREATE USER %s WITH PASSWORD '%s' " % (configData.probe_user, configData.probe_passwd))
            curs.execute("CREATE DATABASE %s WITH owner='probe' " % configData.probe_db)
            curs.execute("ALTER USER %s WITH SUPERUSER" % configData.probe_user)
            curs.execute("ALTER USER %s WITH CREATEDB" % configData.probe_user)
            curs.execute("ALTER USER %s WITH CREATEROLE" % configData.probe_user)
            curs.execute("GRANT ALL ON DATABASE %s TO %s" % (configData.probe_db, configData.probe_user))

        curs.close()
        condb.close()
    except Exception as e:
        logger.error('Exception was raised due to \'%s\' ' % e)
        pass



class DBConnection:
    def __init__(self, dbInfo, process, lock):
        try:
            self.conn = psycopg2.connect(dbInfo)
            self.cur = self.conn.cursor()
            self.process = process
            self.lock = lock
        except Exception as e:
            logger.exception("Exception occured in DBConnection() %s" % e)
            return None

        pass

    def CreateTables(self):
        try :
            createS1 = "CREATE TABLE IF NOT EXISTS S1 (" \
                       " enbIP        varchar(40)," \
                       " mmeIP        varchar(40)," \
                       " enb_ue_ID    varchar(40), " \
                       " mme_ue_ID    varchar(40)," \
                       " s1apType     VARCHAR(40)," \
                       " tmsi         varchar(60), " \
                       " imsi         varchar(60)," \
                       " guti         varchar(60)," \
                       " cellid       varchar(25)," \
                       " res          varchar(50)" \
                       " );"

            self.cur.execute(createS1)

            logger.debug('Created S1 table %s' % createS1)

            callFlowS1 = "CREATE TABLE IF NOT EXISTS CALLFLOW_S1 (" \
                         " pkt_id       INTEGER," \
                         " pkt_time     VARCHAR(100)," \
                         " file_id      VARCHAR(100)," \
                         " enbIP        VARCHAR(40)," \
                         " mmeIP        VARCHAR(40)," \
                         " enb_ue_ID    VARCHAR(40), " \
                         " mme_ue_ID    VARCHAR(40)," \
                         " message      VARCHAR(100)," \
                         " request      VARCHAR(10)" \
                         ");"

            self.cur.execute(callFlowS1)
            logger.debug('Created CallFlow table %s' % callFlowS1)

            createS6 = "CREATE TABLE IF NOT EXISTS S6 (" \
                       " hohid    VARCHAR(40)," \
                       " eoeid    VARCHAR(40)," \
                       " rat      VARCHAR(40)," \
                       " imsi     VARCHAR(60)," \
                       " imei     VARCHAR(60)," \
                       " msisdn   VARCHAR(60)," \
                       " xres     VARCHAR(60)," \
                       " kasme    VARCHAR(400)" \
                       ");"
            self.cur.execute(createS6)
            logger.debug('Created S6 table %s' % createS6)

            callFlowS6 = "CREATE TABLE IF NOT EXISTS CALLFLOW_S6 (" \
                         " pkt_id   INTEGER," \
                         " pkt_time VARCHAR(100)," \
                         " file_id  VARCHAR(100)," \
                         " mmeIP    VARCHAR(40)," \
                         " hssIP    VARCHAR(40)," \
                         " hohid    VARCHAR(40)," \
                         " eoeid    VARCHAR(40)," \
                         " message  VARCHAR(100)," \
                         " request  INTEGER" \
                         ");"

            self.cur.execute(callFlowS6)
            logger.debug('Created CallFlow table %s' % callFlowS6)

            createGTPC = "CREATE TABLE IF NOT EXISTS GTPC (" \
                         " seq_num                          varchar(60)," \
                         " teid                             varchar(60)," \
                         " imsi                             varchar(60)," \
                         " imei                             varchar(60)," \
                         " msisdn                           varchar(60)," \
                         " fteid_1                          varchar(60)," \
                         " fteid_2                          varchar(60)" \
                         " );"

            self.cur.execute(createGTPC)
            logger.debug('Created GTPC table %s' % createGTPC)

            callFlowGTPC = "CREATE TABLE IF NOT EXISTS CALLFLOW_GTPC (" \
                           " pkt_id        INTEGER," \
                           " pkt_time      VARCHAR(60)," \
                           " file_id       VARCHAR(100)," \
                           " seq_num       VARCHAR(60)," \
                           " teid          VARCHAR(60)," \
                           " mmeip         VARCHAR(60)," \
                           " sgwip         VARCHAR(60)," \
                           " request       INTEGER," \
                           " fteid_1       VARCHAR(60)," \
                           " fteid_2       VARCHAR(60)," \
                           " message       VARCHAR(60)" \
                           ");"

            self.cur.execute(callFlowGTPC)
            logger.debug('Created CallFlow table %s' % callFlowGTPC)


        except Exception as e:
            logger.exception("Exception occured in DBConnection::CreateTables %s" % e)



    def CreateViews(self, imsi, msisdn):

        try:
            v1 = "CREATE or REPLACE VIEW V1 AS" \
                 " SELECT" \
                 " CALLFLOW_S1.file_id," \
                 " CALLFLOW_S1.pkt_id," \
                 " CALLFLOW_S1.pkt_time," \
                 " S1.enbip," \
                 " S1.mmeip," \
                 " S1.imsi," \
                 " S1.res," \
                 " CALLFLOW_S1.message," \
                 " CALLFLOW_S1.request" \
                 " FROM S1, CALLFLOW_S1" \
                 " WHERE" \
                 " S1.enbip = CALLFLOW_S1.enbip AND" \
                 " S1.mmeip = CALLFLOW_S1.mmeip AND" \
                 " S1.enb_ue_ID = CALLFLOW_S1.enb_ue_ID" \
                 ";"

            self.cur.execute(v1)
            logger.info('Created View V1 %s' % v1)


            if imsi is not None:
                v1_imsi_only = "CREATE or REPLACE VIEW V1_IMSI_ONLY AS" \
                               " SELECT" \
                               " CALLFLOW_S1.file_id," \
                               " CALLFLOW_S1.pkt_id," \
                               " CALLFLOW_S1.pkt_time," \
                               " S1.enbip," \
                               " S1.mmeip," \
                               " S1.imsi," \
                               " S1.res," \
                               " CALLFLOW_S1.message," \
                               " CALLFLOW_S1.request" \
                               " FROM S1, CALLFLOW_S1" \
                               " WHERE" \
                               " S1.enbip = CALLFLOW_S1.enbip AND" \
                               " S1.mmeip = CALLFLOW_S1.mmeip AND" \
                               " S1.enb_ue_ID = CALLFLOW_S1.enb_ue_ID AND" \
                               " S1.imsi = '" + imsi + "';"

                self.cur.execute(v1_imsi_only)
                logger.info('Created View V1_IMSI_ONLY %s' % v1_imsi_only)

            v6 = "CREATE or REPLACE VIEW V6 AS" \
                 " SELECT " \
                 " CALLFLOW_S6.file_id," \
                 " CALLFLOW_S6.pkt_id," \
                 " CALLFLOW_S6.pkt_time," \
                 " CALLFLOW_S6.mmeip," \
                 " CALLFLOW_S6.hssip," \
                 " S6.imsi," \
                 " S6.xres," \
                 " CALLFLOW_S6.message," \
                 " CALLFLOW_S6.request" \
                 " FROM S6, CALLFLOW_S6" \
                 " WHERE" \
                 " S6.hohid = CALLFLOW_S6.hohid AND" \
                 " S6.eoeid = CALLFLOW_S6.eoeid" \
                 ";"

            self.cur.execute(v6)
            logger.info('Created View V6 %s' % v6)
            if imsi is not None:
                v6_imsi_only = "CREATE or REPLACE VIEW V6_IMSI_ONLY AS" \
                               " SELECT " \
                               " CALLFLOW_S6.file_id," \
                               " CALLFLOW_S6.pkt_id," \
                               " CALLFLOW_S6.pkt_time," \
                               " CALLFLOW_S6.mmeip," \
                               " CALLFLOW_S6.hssip," \
                               " S6.imsi," \
                               " S6.xres," \
                               " CALLFLOW_S6.message," \
                               " CALLFLOW_S6.request" \
                               " FROM S6, CALLFLOW_S6" \
                               " WHERE" \
                               " S6.hohid = CALLFLOW_S6.hohid AND" \
                               " S6.eoeid = CALLFLOW_S6.eoeid AND" \
                               " S6.imsi  = '" + imsi+  "';"

                self.cur.execute(v6_imsi_only)
                logger.info('Created View V6_IMSI_ONLY %s' % v6_imsi_only)

            if msisdn is not None:
                v6_msisdn_only = "CREATE or REPLACE VIEW V6_MSISDN_ONLY AS" \
                               " SELECT " \
                               " CALLFLOW_S6.file_id," \
                               " CALLFLOW_S6.pkt_id," \
                               " CALLFLOW_S6.pkt_time," \
                               " CALLFLOW_S6.mmeip," \
                               " CALLFLOW_S6.hssip," \
                               " S6.imsi," \
                               " S6.xres," \
                               " S6.msisdn," \
                               " CALLFLOW_S6.message," \
                               " CALLFLOW_S6.request" \
                               " FROM S6, CALLFLOW_S6" \
                               " WHERE" \
                               " S6.hohid = CALLFLOW_S6.hohid AND" \
                               " S6.eoeid = CALLFLOW_S6.eoeid AND" \
                               " S6.msisdn  = '" + msisdn + "';"

                self.cur.execute(v6_msisdn_only)
                logger.info('Created View V6_MSISDN_ONLY %s' % v6_msisdn_only)

            vgtpc = "CREATE or REPLACE VIEW VGTPC AS" \
                 " SELECT CALLFLOW_GTPC.file_id," \
                 " CALLFLOW_GTPC.pkt_id," \
                 " CALLFLOW_GTPC.pkt_time," \
                 " CALLFLOW_GTPC.FTEID_1," \
                 " CALLFLOW_GTPC.FTEID_2," \
                 " CALLFLOW_GTPC.mmeip," \
                 " CALLFLOW_GTPC.sgwip," \
                 " GTPC.IMSI," \
                 " GTPC.IMEI," \
                 " GTPC.MSISDN," \
                 " GTPC.seq_num," \
                 " GTPC.teid," \
                 " CALLFLOW_GTPC.message," \
                 " CALLFLOW_GTPC.request" \
                 " FROM GTPC, CALLFLOW_GTPC" \
                 " WHERE" \
                 " GTPC.teid =  CALLFLOW_GTPC.teid AND" \
                 " GTPC.seq_num = CALLFLOW_GTPC.seq_num " \
                 ";"

            self.cur.execute(vgtpc)
            logger.info('Created View VGTPC %s' % vgtpc)

            if imsi is not None:
                vgtpc_imsi_only = "CREATE or REPLACE VIEW VGTPC_IMSI_ONLY AS" \
                                  " SELECT CALLFLOW_GTPC.file_id," \
                                  " CALLFLOW_GTPC.pkt_id," \
                                  " CALLFLOW_GTPC.pkt_time," \
                                  " CALLFLOW_GTPC.FTEID_1," \
                                  " CALLFLOW_GTPC.FTEID_2," \
                                  " CALLFLOW_GTPC.mmeip," \
                                  " CALLFLOW_GTPC.sgwip," \
                                  " GTPC.IMSI," \
                                  " GTPC.IMEI," \
                                  " GTPC.MSISDN," \
                                  " GTPC.seq_num," \
                                  " GTPC.teid," \
                                  " CALLFLOW_GTPC.message," \
                                  " CALLFLOW_GTPC.request" \
                                  " FROM GTPC, CALLFLOW_GTPC" \
                                  " WHERE" \
                                  " GTPC.teid = CALLFLOW_GTPC.teid AND" \
                                  " GTPC.seq_num = CALLFLOW_GTPC.seq_num AND" \
                                  " GTPC.imsi  = '" + imsi + "';"

                self.cur.execute(vgtpc_imsi_only)
                logger.info('Created View VGTPC_IMSI_ONLY %s' % vgtpc_imsi_only)

            if msisdn is not None:
                vgtpc_msisdn_only = "CREATE or REPLACE VIEW VGTPC_MSISDN_ONLY AS" \
                                    " SELECT CALLFLOW_GTPC.file_id," \
                                    " CALLFLOW_GTPC.pkt_id," \
                                    " CALLFLOW_GTPC.pkt_time," \
                                    " CALLFLOW_GTPC.FTEID_1," \
                                    " CALLFLOW_GTPC.FTEID_2," \
                                    " CALLFLOW_GTPC.mmeip," \
                                    " CALLFLOW_GTPC.sgwip," \
                                    " GTPC.IMSI," \
                                    " GTPC.IMEI," \
                                    " GTPC.MSISDN," \
                                    " GTPC.seq_num," \
                                    " GTPC.teid," \
                                    " CALLFLOW_GTPC.message," \
                                    " CALLFLOW_GTPC.request" \
                                    " FROM GTPC, CALLFLOW_GTPC" \
                                    " WHERE" \
                                    " GTPC.teid = CALLFLOW_GTPC.teid AND" \
                                    " GTPC.seq_num = CALLFLOW_GTPC.seq_num AND" \
                                    " GTPC.MSISDN  = '" + msisdn + "';"

                self.cur.execute(vgtpc_msisdn_only)
                logger.info('Created View VGTPC_MSISDN_ONLY %s' % vgtpc_msisdn_only)


            if imsi is not None:
                vclub = "CREATE or REPLACE VIEW VCLUB AS" \
                        " SELECT" \
                        " V1_IMSI_ONLY.file_id," \
                        " V1_IMSI_ONLY.pkt_id," \
                        " V1_IMSI_ONLY.pkt_time," \
                        " V1_IMSI_ONLY.enbip AS node_1," \
                        " V1_IMSI_ONLY.mmeip AS node_2," \
                        " V1_IMSI_ONLY.imsi," \
                        " V1_IMSI_ONLY.res AS xres," \
                        " V1_IMSI_ONLY.message," \
                        " null as seq_num," \
                        " null as teid," \
                        " null as fteid_1," \
                        " null as fteid_2," \
                        " V1_IMSI_ONLY.request FROM V1_IMSI_ONLY" \
                        " UNION" \
                        " SELECT" \
                        " V6_IMSI_ONLY.file_id," \
                        " V6_IMSI_ONLY.pkt_id," \
                        " V6_IMSI_ONLY.pkt_time," \
                        " V6_IMSI_ONLY.mmeip AS node_1," \
                        " V6_IMSI_ONLY.hssip AS node_2," \
                        " V6_IMSI_ONLY.imsi," \
                        " V6_IMSI_ONLY.xres," \
                        " V6_IMSI_ONLY.message," \
                        " null as seq_num," \
                        " null as teid," \
                        " null as fteid_1," \
                        " null as fteid_2," \
                        " V6_IMSI_ONLY.request FROM V6_IMSI_ONLY" \
                        " UNION " \
                        " SELECT" \
                        " VGTPC_IMSI_ONLY.file_id," \
                        " VGTPC_IMSI_ONLY.pkt_id," \
                        " VGTPC_IMSI_ONLY.pkt_time," \
                        " VGTPC_IMSI_ONLY.mmeip AS node_1," \
                        " VGTPC_IMSI_ONLY.sgwip AS node_2," \
                        " VGTPC_IMSI_ONLY.imsi," \
                        " null as xres," \
                        " VGTPC_IMSI_ONLY.message," \
                        " VGTPC_IMSI_ONLY.seq_num," \
                        " VGTPC_IMSI_ONLY.teid," \
                        " VGTPC_IMSI_ONLY.fteid_1," \
                        " VGTPC_IMSI_ONLY.fteid_2," \
                        " VGTPC_IMSI_ONLY.request FROM VGTPC_IMSI_ONLY" \
                        " ORDER BY file_id" \
                        ";"


            elif msisdn is not None:
                vclub = "CREATE or REPLACE VIEW VCLUB AS" \
                        " SELECT " \
                        " V1.file_id," \
                        " V1.pkt_id," \
                        " V1.pkt_time," \
                        " V1.enbip AS node_1," \
                        " V1.mmeip AS node_2," \
                        " Null AS msisdn," \
                        " V1.imsi," \
                        " V1.res AS xres," \
                        " V1.message," \
                        " null as seq_num," \
                        " null as teid," \
                        " null as fteid_1," \
                        " null as fteid_2," \
                        " V1.request FROM V1" \
                        " UNION" \
                        " SELECT " \
                        " V6_MSISDN_ONLY.file_id," \
                        " V6_MSISDN_ONLY.pkt_id," \
                        " V6_MSISDN_ONLY.pkt_time," \
                        " V6_MSISDN_ONLY.mmeip AS node_1," \
                        " V6_MSISDN_ONLY.hssip AS node_2," \
                        " V6_MSISDN_ONLY.msisdn," \
                        " V6_MSISDN_ONLY.imsi," \
                        " V6_MSISDN_ONLY.xres," \
                        " V6_MSISDN_ONLY.message," \
                        " null as seq_num," \
                        " null as teid," \
                        " null as fteid_1," \
                        " null as fteid_2," \
                        " V6_MSISDN_ONLY.request FROM V6_MSISDN_ONLY" \
                        " UNION " \
                        " SELECT" \
                        " VGTPC_MSISDN_ONLY.file_id," \
                        " VGTPC_MSISDN_ONLY.pkt_id," \
                        " VGTPC_MSISDN_ONLY.pkt_time," \
                        " VGTPC_MSISDN_ONLY.mmeip AS node_1," \
                        " VGTPC_MSISDN_ONLY.sgwip AS node_2," \
                        " VGTPC_MSISDN_ONLY.msisdn," \
                        " VGTPC_MSISDN_ONLY.imsi," \
                        " null as xres," \
                        " VGTPC_MSISDN_ONLY.message," \
                        " VGTPC_MSISDN_ONLY.seq_num," \
                        " VGTPC_MSISDN_ONLY.teid," \
                        " VGTPC_MSISDN_ONLY.fteid_1," \
                        " VGTPC_MSISDN_ONLY.fteid_2," \
                        " VGTPC_MSISDN_ONLY.request FROM VGTPC_MSISDN_ONLY" \
                        " ORDER BY file_id" \
                        ";"

            else:
                vclub = "CREATE or REPLACE VIEW VCLUB AS" \
                        " SELECT" \
                        " V1.file_id," \
                        " V1.pkt_id," \
                        " V1.pkt_time," \
                        " V1.enbip AS node_1," \
                        " V1.mmeip AS node_2," \
                        " V1.imsi," \
                        " V1.res AS xres," \
                        " V1.message," \
                        " null as seq_num," \
                        " null as teid," \
                        " null as fteid_1," \
                        " null as fteid_2," \
                        " V1.request FROM V1" \
                        " UNION" \
                        " SELECT" \
                        " V6.file_id," \
                        " V6.pkt_id," \
                        " V6.pkt_time," \
                        " V6.mmeip AS node_1," \
                        " V6.hssip AS node_2," \
                        " V6.imsi," \
                        " V6.xres," \
                        " V6.message," \
                        " null as seq_num," \
                        " null as teid," \
                        " null as fteid_1," \
                        " null as fteid_2," \
                        " V6.request FROM V6" \
                        " UNION " \
                        " SELECT" \
                        " VGTPC.file_id," \
                        " VGTPC.pkt_id," \
                        " VGTPC.pkt_time," \
                        " VGTPC.mmeip AS node_1," \
                        " VGTPC.sgwip AS node_2," \
                        " VGTPC.imsi," \
                        " null as xres," \
                        " VGTPC.message," \
                        " VGTPC.seq_num," \
                        " VGTPC.teid," \
                        " VGTPC.fteid_1," \
                        " VGTPC.fteid_2," \
                        " VGTPC.request FROM VGTPC" \
                        " ORDER BY file_id" \
                        ";"

            self.cur.execute(vclub)
            logger.info('Created View VCLUB %s' % vclub)

        except Exception as e:
            logger.exception("Exception occured in DBConnection::CreateViews %s" % e)


    def CreateAllElements(self):
        try :
            #self.DropTables()
            if self.lock:
                self.lock.acquire()
                self.CreateTables()
                self.lock.release()
                self.Commit()

        except Exception as e:
            logger.exception("Exception occured in DBConnection::CreateAllElements %s" % e)
            exit(1)

    def CreateAllViews(self, imsi, msisdn):
        try:
            # self.DropViews()
            if imsi is not None:
                self.CreateViews(imsi, None)
            elif msisdn is not None:
                self.CreateViews(None, msisdn)
            else:
                self.CreateViews(None, None)

            self.Commit()

        except Exception as e:
            logger.exception("Exception occured in DBConnection::CreateAllViews %s" % e)


    def DropTables(self):
        try:
            tables = ['S1', 'CALLFLOW_S1',
                      'S6', 'CALLFLOW_S6',
                      'GTPC', 'CALLFLOW_GTPC']

            for table in tables:
                drop = "DROP TABLE IF EXISTS %s;" % table
                self.Execute(drop)

        except Exception as e:
            logger.exception("Exception occured in DBConnection::DropTables %s" % e)
            exit(1)

    def DropViews(self):
        try:
            views = ["VCLUB", "V1", "V6", "V1_IMSI_ONLY", "V6_IMSI_ONLY", "V6_MSISDN_ONLY", "VGTPC", "VGTPC_IMSI_ONLY", "VGTPC_MSISDN_ONLY"]
            for view in views:
                drop = "DROP VIEW IF EXISTS %s;" % view
                self.Execute(drop)

        except Exception as e:
            logger.exception("Exception occured in DBConnection::DropTables %s" % e)
            exit(1)

    def Query(self, queryStr):
        self.cur.execute()

    def CheckConnection(self):
        pass

    def Close(self):
        self.cur.close()
        self.conn.close()

    def Commit(self):
        self.conn.commit()

    def Execute(self, queryStr):
        self.cur.execute(queryStr)

    def RowCount(self):
        return self.cur.rowcount

    def Cursor(self):
        return self.cur


# FIXME: This call should be more generic, than specific to S1. Hence ugly.
# FIXME: Rework on this is required.
class SQLS1Operation:
    def __init__(self, sqlConnection):
        self.sql = sqlConnection
        pass

    def isConnected(self):
        pass

    def getSqlConnection(self):
        return self.sql

    def Query(self, queryStr):
        pass

    def finalRecordCount(self):
        tables = ['S1', 'CALLFLOW_S1']
        for table in tables:
            self.sql.Execute("SELECT count(*) from %s" % table)
            cnt = self.sql.RowCount()
            if cnt:
                d = self.sql.Cursor().fetchall()
                if len(d):
                    logger.info("%s recorded %s entires in db" % (table, d[0][0]))
                else:
                    logger.info("S1Operation, could not get any records from db")


    def merge(self, fields, data):
        # FIXME: Think a more better way to deal here. This is not advisible. Make it more generic.
        fields[0] = fields[0] if len(fields[0]) else data[0]
        fields[1] = fields[1] if len(fields[1]) else data[1]
        fields[2] = fields[2] if fields[2] > 0 else data[2]
        fields[3] = fields[3] if fields[3] > 0 else data[3]
        fields[4] = fields[4] if fields[4] > 0 else data[4]
        fields[5] = fields[5] if len(fields[5]) else data[5]
        fields[6] = fields[6] if len(fields[6]) else data[6]
        fields[7] = fields[7] if len(fields[7]) else data[7]
        fields[8] = fields[8] if len(fields[8]) else data[8]
        fields[9] = fields[9] if len(fields[9]) else data[9]
        fields[10] = fields[10] if len(fields[10]) else data[10]
        fields[11] = fields[11] if len(fields[11]) else data[11]
        return fields


    def Insert_or_Update(self, table, fields):
        updateStr = ''
        selectStr = ''
        ret = 0
        data = ()

        # logger.info(fields)

        # FIXME: Getting the values from the fields and filling this way is really expensive and not recommended.
        # https://www.slideshare.net/petereisentraut/programming-with-python-and-postgresql

        insertStr = """INSERT INTO %s VALUES (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\',
                                              \'%s\', \'%s\', \'%s\', \'%s\');"""\
                                              % (table, fields[0], fields[1], fields[2], fields[3], fields[4],
                                                 fields[5], fields[6], fields[7], fields[8], fields[9])

        try:
            self.sql.Execute(insertStr)
        except Exception as e:
            logger.exception("Exception occured in SQLS1Operation::insert_or_update() %s" % e)


    # This is used only by CALLFLOW_S1
    def Insert(self, table, fields):
        insertStr = "INSERT INTO %s VALUES (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\'," \
                    "\'%s\', \'%s\', \'%s\', \'%s\');" \
                    % (table, fields[0], fields[1], fields[2], fields[3],
                       fields[4], fields[5], fields[6], fields[7], fields[8])

        try:
            self.sql.Execute(insertStr)
        except Exception as e:
            logger.exception("Exception occured in SQLS1Operation::Insert() %s" % e)


class SQLS6Operation:
    def __init__(self, sqlConnection):
        self.sql = sqlConnection
        pass

    def getSqlConnection(self):
        return self.sql


    def finalRecordCount(self):
        tables = ['S6', 'CALLFLOW_S6']
        for table in tables:
            self.sql.Execute("SELECT count(*) from %s" % table)
            cnt = self.sql.RowCount()
            if cnt:
                d = self.sql.Cursor().fetchall()
                if len(d):
                    logger.info("%s recorded %s entires in db" % (table, d[0][0]))
                else:
                    logger.info("S6Operation yeilded no entries in db")


    def merge(self, new, old):
        fields = old
        fields[0] = new[0] if len(new[0]) else old[0]
        fields[1] = new[1] if len(new[1]) else old[1]
        fields[2] = new[2] if len(new[2]) else old[2]
        fields[3] = new[3] if len(new[3]) else old[3]
        fields[4] = new[4] if len(new[4]) else old[4]
        fields[5] = new[5] if len(new[5]) else old[5]
        fields[6] = new[6] if len(new[6]) else old[6]
        fields[7] = new[7] if len(new[7]) else old[7]

        return fields



    # This is used only by CALLFLOW_S6
    def Insert(self, table, fields):
        insertStr = "INSERT INTO %s VALUES (\'%s\', \'%s\', \'%s\', \'%s\'," \
                    " \'%s\', \'%s\', \'%s\', \'%s\', \'%s\');" \
                    % (table, fields[0], fields[1], fields[2], fields[3], fields[4],
                       fields[5], fields[6], fields[7], fields[8])

        try:
            self.sql.Execute(insertStr)
        except Exception as e:
            logger.exception("Exception occured in SQLS6Operation::Insert() %s" % e)


    def Insert_Or_Update(self, table, fields):
        insertStr = "INSERT INTO %s VALUES (\'%s\', \'%s\', \'%s\', \'%s\',\'%s\', \'%s\', \'%s\', \'%s\');" \
                    % (table, fields[0], fields[1], fields[2], fields[3], fields[4], fields[5], fields[6], fields[7])

        try:
            self.sql.Execute(insertStr)
        except Exception as e:
            logger.exception("Exception occured in SQLS6Operation::Insert_or_update() %s" % e)
            print (e)


class SQLGTPCOperation:
    def __init__(self, sqlConnection):
        self.sql = sqlConnection
        pass

    def getSqlConnection(self):
        return self.sql


    def finalRecordCount(self):
        tables = ['GTPC', 'CALLFLOW_GTPC']
        for table in tables:
            self.sql.Execute("SELECT count(*) from %s" % table)
            cnt = self.sql.RowCount()
            if cnt:
                d = self.sql.Cursor().fetchall()
                if len(d):
                    logger.info("%s recorded %s entires in db" % (table, d[0][0]))
                else:
                    logger.info("GTPCOperation yeilded no entries in db")



    # This is used only by CALLFLOW_GTPC
    def Insert(self, table, fields):

        insertStr = "INSERT INTO %s VALUES (\'%d\', \'%s\'," \
                    " \'%s\', \'%s\', \'%s\'," \
                    "\'%s\', \'%s\', \'%d\'," \
                    "\'%s\', \'%s\', \'%s\');"\
                    % (table, fields[0], fields[1], fields[2], fields[3], fields[4],
                        fields[5], fields[6], fields[7],
                        fields[8], fields[9], fields[10])
        # , \'%s\', \'%s\', \'%d\', \'%s\', \'%s\');"\
        try:
            self.sql.Execute(insertStr)
        except Exception as e:
            logger.exception("Exception occured in SQLGTPCOperation::Insert() %s" % e)


    def Insert_Or_Update(self, table, fields):
        insertStr = "INSERT INTO %s VALUES (\'%s\', \'%s\', " \
                    "\'%s\', \'%s\', \'%s\', \'%s\', \'%s\');" \
                    % (table, fields[0], fields[1], fields[2], fields[3], fields[4], fields[5], fields[6])


        try:
            self.sql.Execute(insertStr)
        except Exception as e:
            logger.exception("Exception occured in SQLGTPCOperation::Insert_or_update() %s" % e)
            print (e)

