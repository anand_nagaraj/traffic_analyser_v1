import re

from seqdiag import parser, builder, drawer

class SeqPlotter:
    def __init__(self, srcfname, path):
        self.srcfname = srcfname
        self.path = path

    def GenerateCallGraph(self):

        try:
            fileHandle = open(self.srcfname, 'r')
            diaStr = ''
            diagramList = []
            for line in fileHandle:
                fields = line.split(',')
                if len(fields) == 1:
                    diaStr += fields[0]
                else:
                    diaStr += fields[0]
                    diagramList.append(diaStr)
                    diaStr = ''


            for oneSeq in diagramList:
                oneSeq = """seqdiag {
                        default_fontsize = 16;
                        edge_length = 500;  // default value is 192
                        span_height = 100;  // default value is 40
                        // Numbering edges automaticaly
                        autonumber = True;
                        activation = none;	    
                        ===  IMSI = "311480252936977" ===           
		                10.55.3.34 <- 10.48.0.40 [label = "id-downlinkNASTransport"]
		                10.55.3.34 -> 10.48.0.40 [label = "id-initialUEMessage"];
		                10.55.3.34 -> 10.48.0.40 [label = "id-UEContextRelease"];
		                10.55.3.34 -> 10.48.0.40 [label = "id-initialUEMessage"];
                        }"""
                #print oneSeq
                m = re.search('note = \".*?\"', oneSeq).group(0)
                if m:
                    fields = m.split('\"')
                    key = fields[1]
                    tree = parser.parse_string(oneSeq)
                    diagram = builder.ScreenNodeBuilder.build(tree)
                    draw = drawer.DiagramDraw('PNG', diagram, filename=self.path + str(key) + ".png")
                    draw.draw()
                    draw.save()
                    return

        except Exception as e:
            print (e)



if __name__ == "__main__":
    seq = SeqPlotter('/home/anagaraj/seqDiagram.txt', '/tmp/Diagram/Diatest_')

    seq.GenerateCallGraph()