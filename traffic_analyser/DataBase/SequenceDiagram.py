import logging
import pickle
import os
from multiprocessing import Process, Pipe
from seqdiag import parser, builder, drawer

logger = logging.getLogger('')

def check_folder_exists(path):
    import os

    directory = os.path.dirname(path)
    if not os.path.exists(directory):
        os.makedirs(directory)


class SeqDiagramConfig:
    def __init__(self, sqlConn, seqDiaPath, src_pcap_path, dst_pcap_path,
                 filt, imsi_png_pop, imsi_pcap_pop, pcapwriters):
        self.seqDiaPath = seqDiaPath
        self.src_ppath = src_pcap_path
        self.dst_ppath = dst_pcap_path
        self.sql = sqlConn
        self.filter = filt
        self.imsi_png_pop = imsi_png_pop
        self.imsi_pcap_pop = imsi_pcap_pop
        self.num_pcap_writer = pcapwriters

        if seqDiaPath:
            check_folder_exists(seqDiaPath)
        if dst_pcap_path:
            check_folder_exists(dst_pcap_path)

class SeqDiagramData:
    def __init__(self, type = '', imsi = '', filemap = {}, diagram = ''):
        self.type = type
        self.imsi = imsi
        self.filemap = filemap
        self.diagram_definition = diagram
        pickle.HIGHEST_PROTOCOL

    def get(self):
        return [self.type, self.imsi, self.filemap, self.diagram_definition]

    def pickle_pack(self):
        return pickle.dumps(self.get())

    def pickle_unpack(self, msg):
        return pickle.loads(msg)


class SeqDiagramPlotter:
    def __init__(self, seqConfig):
        self.seqConfig = seqConfig
        self.start_seq = "seqdiag {\n" \
                        "\tdefault_fontsize = 13;\n" \
                        "\tdefault_node_color = yellow;\n" \
                        "\tedge_length = 500;\n" \
                        "\tspan_height = 50;\n" \
                        "\tautonumber = True;\n" \
                        "\tactivation = none;\n"
        self.colorcodes = ['yellow', 'lightgreen', 'lightpink', 'yellow', 'lightblue',
                           'orange', 'purple', 'red', 'brown', 'lightpink', 'orange',
                           'yellow', 'lightgreen', 'lightblue', 'red', 'green']

        self.pcap_writer = []
        self.carrier = []

    def initialize_workers(self):
        if self.seqConfig.dst_ppath:
            self.CreatePcapWriterWorker()
            return self.pcap_writer


    def WritePcapForIMSI(self, imsi, filemap, diagram_definition):
        fname = self.seqConfig.dst_ppath + "/" + imsi + ".pcap"
        if os.path.exists(fname):
            os.remove(fname)

        tree = parser.parse_string(diagram_definition)
        diagram = builder.ScreenNodeBuilder.build(tree)
        draw = drawer.DiagramDraw('PNG', diagram, filename=self.seqConfig.seqDiaPath + imsi + ".png")
        draw.draw()
        draw.save()

        if imsi in self.seqConfig.imsi_png_pop:
            cmd = "eog -n " + self.seqConfig.seqDiaPath + imsi + ".png & "
            logger.info(cmd)
            os.system(cmd)

        spath = self.seqConfig.src_ppath
        dpath = self.seqConfig.dst_ppath
        filt = " -" + self.seqConfig.filter + " "

        for file, packets_list in filemap.items():
            packets = list(set(packets_list))
            logger.info("%s packets found in ==> %s : %s", imsi, file, packets)
            actual_filename = dpath + "/" + imsi + ".pcap"
            check_file_exists = os.path.exists(actual_filename)

            if not check_file_exists:
                cmd = "tshark -r " + spath + "/" + file + filt + "\" "
                for i in range(len(packets)):
                    cmd += "frame.number==" + str(packets[i])
                    if i != len(packets) - 1:
                        cmd += " || "
                    else:
                        cmd += " \" -w " + actual_filename
                logger.info(cmd)
                os.system(cmd)
            else:
                # Only used for merging.
                tmp_fname1 = actual_filename + ".tmp1.pcap"
                tmp_fname2 = actual_filename + ".tmp2.pcap"
                os.rename(actual_filename, tmp_fname1)
                actual_filename = dpath + "/" + imsi + ".pcap"
                cmd = "tshark -r " + spath + "/" + file + filt + "\" "
                for i in range(len(packets)):
                    cmd += "frame.number==" + str(packets[i])
                    if i != len(packets) - 1:
                        cmd += " || "
                    else:
                        cmd += " \" -w " + tmp_fname2
                logger.info(cmd)
                os.system(cmd)
                merge_cmd = "mergecap -w " + actual_filename + " " + tmp_fname1 + " " + tmp_fname2
                logger.info("Merging pcap files %s" % merge_cmd)
                os.system(merge_cmd)
                os.remove(tmp_fname1)
                os.remove(tmp_fname2)


    def CreatePcapWriterWorker(self):
        for ind in range(self.seqConfig.num_pcap_writer):
            output_p, input_p = Pipe()
            if output_p is not None and input_p is not None:
                tup = (output_p, input_p)
                self.carrier.append(tup)

            work_name = 'PcapWriter-' + str(ind)
            worker = Process(name=work_name, target=self.StartPcapWriterWorker,
                             args=(work_name, self.carrier[ind]))

            self.pcap_writer.append(worker)

        for ind in range(len(self.pcap_writer)):
            worker = self.pcap_writer[ind]
            worker.start()

    def correlated_gtpc_data_per_imsi(self, data):
        final_query = []
        lsofseq = []
        lsofteids = []
        dict_imsi_teid = {}
        dict_seq_imsi = {}
        for i in range(len(data)):
            if data[i][5] != '' and (data[i][7] == 'CreateSessionRequest') or (
                    data[i][7] == 'ForwardRelocationRequest'):
                 dict_imsi_teid[data[i][5]] = []
                 dict_seq_imsi[data[i][8]] = data[i][5]
                 logger.info("seq no = %s and imsi = %s" % (data[i][8], data[i][5]))
            final_query.append(data[i])

        for k, v in dict_seq_imsi.items():
            logger.info("seq = %s imsi = %s" % (k, v))
            for j in range(len(data)):
                if data[j][8] == k and (data[j][7] == 'CreateSessionResponse' or data[j][7] == 'ForwardRelocationResponse') :
                    if data[j][10] != '':
                        dict_imsi_teid[v].append(data[j][10])
                    if data[j][11] != '':
                        dict_imsi_teid[v].append(data[j][11])
                #logger.info("fteids = %s",dict_imsi_teid[v])
        for k, v in dict_imsi_teid.items():
            logger.info("fteids kv  = %s and imsi = %s and teid = %s" % (dict_imsi_teid[k], k, v))
            for it in range(len(v)):
                for j in range(len(data)):
                    if data[j][9] == v[it] and data[j][7] != 'CreateSessionRequest':
                        tmp_lst = []
                        tmp_lst.append(k)
                        tup = tuple(tmp_lst)
                        tup3 = data[j][0:5] + tup[-1:] + data[j][6:]
                        final_query.append(tup3)
                        del tup3
                        del tup
                        del tmp_lst
        #logger.info("message = %s ",final_query)
        return final_query


    def StartPcapWriterWorker(self, work_name, carrier):

        seqDiagData = SeqDiagramData()
        output_p, input_p = carrier
        input_p.close()
        while True:
            try:
                msg = output_p.recv()

                type, imsi, filemap, diagram_definition = seqDiagData.pickle_unpack(msg)
                if type == 'writePcap':
                    self.WritePcapForIMSI(imsi, filemap, diagram_definition)
                elif type == 'EOF':
                    raise EOFError

            except EOFError:
                logger.info("Received last messages so far on this worker %s " %work_name)
                break



    def GenerateCallGraphViaViews(self, msisdn, mmeiplist):
        if msisdn is None:
            queryStr = "SELECT * FROM VCLUB;"
        else:
            queryStr = "SELECT * FROM VCLUB where msisdn = '" + msisdn + "';"


        try:
            self.seqConfig.sql.Execute(queryStr)
            data = self.seqConfig.sql.Cursor().fetchall()   #list of tuples
            data = self.correlated_gtpc_data_per_imsi(data)

            data = list(sorted(set(data)))

            data_map = {}
            for el in data:
                key = el[5]
                if key != '':
                    data_map[key] = []

            for el in data:
                key = el[5]
                if key != '':
                    data_map[key].append(el)

            ind = 0
            for key, values in data_map.items():
                if msisdn is None:
                    diagram_definition = self.start_seq + "\t=== IMSI = \"%s\" ===\n\n" % key
                else:
                    diagram_definition = self.start_seq + "\t=== MSISDN = \"%s\" ===\n\n" % key


                file_map = {}
                nodes = []
                for el in values:
                    file = el[0]
                    if file not in file_map:
                        file_map[file] = []
                    file_map[file].append(el[1])

                    if msisdn is None:
                        dir = "->" if el[12] else "<-"
                    else:
                        dir = "->" if el[13] else "<-"
                    color = 'red' if dir == "->" else "blue"

                    srcnode = el[3]
                    dstnode = el[4]
                    if srcnode in mmeiplist:
                        srcnode += "_MME"
                    if dstnode in mmeiplist:
                        dstnode += "_MME"

                    if msisdn is None:
                        diagram_definition += "\t%s %s %s [label = \"%s\", color = \"%s\"," \
                                              " leftnote = \"%s %s (%s)\"];\n"\
                                              % (srcnode, dir, dstnode, el[7], color, el[0], el[1], el[2])
                    else:
                        diagram_definition += "\t%s %s %s [label = \"%s\", color = \"%s\"," \
                                              " leftnote = \"%s %s (%s)\"];\n"\
                                              % (srcnode, dir, dstnode, el[8], color, el[0], el[1], el[2])

                    # When multiple nodes i.e multiple enb's are talking b/w multiple mme's this option will be handy.
                    if el[3] not in nodes:
                        nodes.append(el[3])
                    if el[4] not in nodes:
                        nodes.append(el[4])

                diagram_definition += "\n\t"
                for i in range(len(nodes)):
                    node = nodes[i]
                    if nodes[i] in mmeiplist:
                        node += "_MME"
                    diagram_definition += node + "[color=" + self.colorcodes[i] + "];"

                diagram_definition += """\n}\n"""
                # logger.info(el)



                if self.seqConfig.dst_ppath:
                    if ind >= self.seqConfig.num_pcap_writer:
                        ind = 0
                    sqDiagData = SeqDiagramData('writePcap', key, file_map, diagram_definition)
                    msg_tup = sqDiagData.pickle_pack()
                    output_p, input_p = self.carrier[ind]
                    input_p.send(msg_tup)
                    ind += 1

            # Terminate all connections to PCAPWriters
            for ind in range(len(self.carrier)):
                sqDiagData = SeqDiagramData('EOF')
                msg_tup = sqDiagData.pickle_pack()
                output_p, input_p = self.carrier[ind]
                input_p.send(msg_tup)


        except Exception as e:
            logger.exception("Exception occured in SeqDiagramPlotter::GenerateCallGraphViaViews() %s" % e)
