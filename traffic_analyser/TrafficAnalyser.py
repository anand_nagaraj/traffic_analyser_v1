
import logging
import pyshark
import time
import multiprocessing

from DataBase.PostGreSQLDB import *
from DataBase.SequenceDiagram import *
from Layers.DiameterLayer import *
from Layers.SCTPLayer import *
from MultiProcessing import *
# from MultiThreading import *


logger = logging.getLogger('')
processors = ['S1AP', 'DIAMETER', 'DATA']
# processors = ['S1AP', 'DIAMETER']

def human_readable(diff):
    hours = int((diff // 3600) % 24)
    minutes = int((diff // 60) % 60)
    seconds = diff % 60
    return ("%s:%s:%2.6s" %(hours, minutes, seconds))

def getCounters(pkt_cnt, old_pkt_cnt, procs, mainCollector, collectorCarrier):
    procs.PushWorkerToAdditionalWork('getCounters')
    # logger.info('Parsed packets: %s' % pkt_cnt)
    tup = ("TA", None, pkt_cnt - old_pkt_cnt)
    serialized_data = mainCollector.pickle_pack(tup)
    collectorCarrier.put(serialized_data)


def MultiProcessingFunctionality(configData, dblock, mainCollector, collectorCarrier, index, fname):
    procs = MultiProcessor(processors, configData)
    procs.CreatePipes()
    ret = procs.CreateAllReceiverWorkers(configData, dblock, mainCollector, collectorCarrier, index, fname)
    if ret:
        logger.error('Could not start all receiver worker ret status = %s' % ret)
        return None

    procs.StartAllWorkers()

    return procs


class TrafficAnalyser():
    def __init__(self, startTime, sqlConnection, configData, mainCollector, collectorCarrier):
        self.sqlConnection = sqlConnection
        self.configData = configData
        self.startTime = startTime
        self.mainCollector = mainCollector
        self.collectorCarrier = collectorCarrier
        self.old_pkt_cnt = 0


    def PrepareTAWorker(self, index, dblock):
        namevar = 'TA-Worker_' + str(index)
        worker = Process(name=namevar, target=self.StartProcessingFile,
                         args=(dblock, self.mainCollector, self.collectorCarrier, index,))
        worker.start()
        return worker


    def StartProcessingFile(self, dblock, mainCollector, collectorCarrier, index):
        for fname in self.configData.pcap_files:
            self.process_file(fname, dblock, mainCollector, collectorCarrier, index)

    def process_file(self, fname, dblock, mainCollector, collectorCarrier, index):
        packets = pyshark.FileCapture(self.configData.pcap_path + "/" + fname)

        thr = MultiProcessingFunctionality(self.configData, dblock, mainCollector,
                                           collectorCarrier, index, fname)

        if thr is None:
            logger.info('Cannot start multiprocessing.. Please debug.')
            return

        logger.info('Now starting to fetch packets from file %s' % fname)
        pkt_count = 0
        if self.old_pkt_cnt > pkt_count:
            self.old_pkt_cnt = 0
        prev = time.time()
        prevdb = prev
        for packet in packets:
            pkt_count += 1
            logger.debug("%s %s" % (frame_level(packet, pkt_count), packet.layers))
            thr.PushPacketToWorker(packet, pkt_count)
            cur = time.time()
            if cur > prev + 10:
                getCounters(pkt_count, self.old_pkt_cnt, thr, mainCollector, collectorCarrier)
                prev = cur
                self.old_pkt_cnt = pkt_count

                if cur > prevdb + self.configData.db_commit_time:  # Dump only every 1 min
                    thr.PushWorkerToAdditionalWork('dbCommit')
                    prevdb = cur


        getCounters(pkt_count, self.old_pkt_cnt, thr, mainCollector, collectorCarrier)

        thr.PushWorkerToAdditionalWork('dbCommit')
        thr.PushWorkerToAdditionalWork('final')

        # Winding up
        # thr.PushWorkerToAdditionalWork('dbClose')
        thr.PushWorkerToAdditionalWork('EOF')
        thr.CloseAllInputDescriptor()
        thr.WaitUntilAllWorkersFinished()

