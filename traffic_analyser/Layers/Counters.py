
class S1Counters:
    def __init__(self, displayAll = False):
        self.InitialUEMessage = 0
        self.InitialContextSetupRequest = 0
        self.InitialContextSetupResponse = 0
        self.DownlinkNASTransport = 0
        self.E_RABModifyRequest = 0
        self.E_RABReleaseCommand = 0
        self.E_RABSetupRequest = 0
        self.E_RABSetupResponse = 0
        self.HandoverCommand = 0
        self.HandoverFailure = 0
        self.HandoverNotify = 0
        self.HandoverCancel = 0
        self.HandoverCancelAcknowledge = 0
        self.HandoverPreparationFailure = 0
        self.HandoverRequest = 0
        self.HandoverRequestAcknowledge = 0
        self.HandoverRequired = 0
        self.NASNonDeliveryIndication = 0
        self.PathSwitchRequest = 0
        self.PathSwitchRequestAcknowledge = 0
        self.PathSwitchRequestFailure = 0
        self.UEContextReleaseRequest = 0
        self.UEContextReleaseCommand = 0
        self.UEContextReleaseComplete = 0
        self.UplinkNASTransport = 0
        self.Paging = 0
        self.UECapabilityInfoIndication = 0
        self.Others = 0
        self.full_display = displayAll
        self.layer = 0



    def __str__(self):
        ret = "\nS1Counters: Layers = %s\n" % self.layer
        msg = ''
        if self.full_display:
            msg = ("\tInitialUEMessage = %s\n\tInitialContextSetupRequest = %s\n\tInitialContextSetupResponse = %s\n"
                  "\tDownlinkNASTransport = %s\n\tE_RABModifyRequest = %s\n\tE_RABReleaseCommand = %s\n\tE_RABSetupRequest = %s\n\t"
                  "E_RABSetupResponse = %s\n\tHandoverCommand = %s\n\tHandoverFailure = %s\n\tHandoverNotify = %s\n\t"
                  "HandoverCancel = %s\n\tHandoverCancelAcknowledge = %s\n\tHandoverPreparationFailure = %s\n\t"
                  "HandoverRequest = %s\n\tHandoverRequestAcknowledge = %s\n\tHandoverRequired=%s\n\t"
                  "NASNonDeliveryIndication = %s\n\tPathSwitchRequest = %s\n\tPathSwitchRequestAcknowledge = %s\n\t"
                  "PathSwitchRequestFailure = %s\n\tUEContextReleaseRequest = %s\n\tUEContextReleaseCommand = %s\n\t"
                  "UEContextReleaseComplete = %s\n\tUplinkNASTransport = %s\n\tPaging = %s\n\tOthers = %s\n"
                 % (self.InitialUEMessage, self.InitialContextSetupRequest, self.InitialContextSetupResponse,
                    self.DownlinkNASTransport, self.E_RABModifyRequest, self.E_RABReleaseCommand, self.E_RABSetupRequest,
                    self.E_RABSetupResponse, self.HandoverCommand, self.HandoverFailure, self.HandoverNotify, self.HandoverCancel,
                    self.HandoverCancelAcknowledge, self.HandoverPreparationFailure, self.HandoverRequest,
                    self.HandoverRequired, self.HandoverRequestAcknowledge, self.NASNonDeliveryIndication,
                    self.PathSwitchRequest, self.PathSwitchRequestAcknowledge, self.PathSwitchRequestFailure,
                    self.UEContextReleaseRequest, self.UEContextReleaseCommand, self.UEContextReleaseComplete,
                    self.UplinkNASTransport, self.Paging, self.Others))

        else:
            if self.InitialUEMessage:
                msg += ("\t%35s = %s\n" % ('InitialUEMessage', self.InitialUEMessage))
            if self.InitialContextSetupRequest:
                msg += ("\t%35s = %s\n" % ('InitialContextSetupRequest', self.InitialContextSetupRequest))
            if self.InitialContextSetupResponse:
                msg += ("\t%35s = %s\n" % ('InitialContextSetupResponse', self.InitialContextSetupResponse))
            if self.DownlinkNASTransport:
                msg += ("\t%35s = %s\n" % ('DownlinkNASTransport', self.DownlinkNASTransport))
            if self.UplinkNASTransport:
                msg += ("\t%35s = %s\n" % ('UplinkNASTransport', self.UplinkNASTransport))
            if self.E_RABModifyRequest:
                msg += ("\t%35s = %s\n" % ('E_RABModifyRequest', self.E_RABModifyRequest))
            if self.E_RABReleaseCommand:
                msg += ("\t%35s = %s\n" % ('E_RABReleaseCommand', self.E_RABReleaseCommand))
            if self.E_RABSetupRequest:
                msg += ("\t%35s = %s\n" % ('E_RABSetupRequest', self.E_RABSetupRequest))
            if self.E_RABSetupRequest:
                msg += ("\t%35s = %s\n" % ('E_RABSetupResponse', self.E_RABSetupResponse))
            if self.HandoverCommand:
                msg += ("\t%35s = %s\n" % ('HandoverCommand', self.HandoverCommand))
            if self.HandoverFailure:
                msg += ("\t%35s = %s\n" % ('HandoverFailure', self.HandoverFailure))
            if self.HandoverNotify:
                msg += ("\t%35s = %s\n" % ('HandoverNotify', self.HandoverNotify))
            if self.HandoverCancel:
                msg += ("\t%35s = %s\n" % ('HandoverCancel', self.HandoverCancel))
            if self.HandoverCancelAcknowledge:
                msg += ("\t%35s = %s\n" % ('HandoverCancelAcknowledge', self.HandoverCancelAcknowledge))
            if self.HandoverPreparationFailure:
                msg += ("\t%35s = %s\n" % ('HandoverPreparationFailure', self.HandoverPreparationFailure))
            if self.HandoverRequest:
                msg += ("\t%35s = %s\n" % ('HandoverRequest', self.HandoverRequest))
            if self.HandoverRequestAcknowledge:
                msg += ("\t%35s = %s\n" % ('HandoverRequestAcknowledge', self.HandoverRequestAcknowledge))
            if self.HandoverRequired:
                msg += ("\t%35s = %s\n" % ('HandoverRequired', self.HandoverRequired))
            if self.NASNonDeliveryIndication:
                msg += ("\t%35s = %s\n" % ('NASNonDeliveryIndiation', self.NASNonDeliveryIndication))
            if self.PathSwitchRequest:
                msg += ("\t%35s = %s\n" % ('PathSwitchRequest', self.PathSwitchRequest))
            if self.PathSwitchRequestAcknowledge:
                msg += ("\t%35s = %s\n" % ('PathSwitchRequestAcknowledge', self.PathSwitchRequestAcknowledge))
            if self.PathSwitchRequestFailure:
                msg += ("\t%35s = %s\n" % ('PathSwitchRequestFailure', self.PathSwitchRequestFailure))
            if self.UEContextReleaseRequest:
                msg += ("\t%35s = %s\n" % ('UEContextReleaseRequest', self.UEContextReleaseRequest))
            if self.UEContextReleaseCommand:
                msg += ("\t%35s = %s\n" % ('UEContextReleaseCommand', self.UEContextReleaseCommand))
            if self.UEContextReleaseComplete:
                msg += ("\t%35s = %s\n" % ('UEContextReleaseComplete', self.UEContextReleaseComplete))
            if self.Paging:
                msg += ("\t%35s = %s\n" % ('Paging', self.Paging))
            if self.UECapabilityInfoIndication:
                msg += ("\t%35s = %s\n" % ('UECapabilityInfoIndication', self.UECapabilityInfoIndication))
            if self.Others:
                msg += ("\t%35s = %s\n" % ('Others', self.Others))

        return ret + msg


class S6Counters:
    def __init__(self, displayAll = False):
        self.full_display = displayAll
        self.AuthenticationInformationRequest = 0
        self.AuthenticationInformationAnswer = 0
        self.UpdateLocationRequest = 0
        self.UpdateLocationAnswer = 0
        self.InsertSubscriberDataRequest = 0
        self.InsertSubscriberDataAnswer = 0
        self.PurgeUERequest = 0
        self.PurgeUEAnswer = 0
        self.IdentityCheckRequest = 0
        self.IdentityCheckAnswer = 0
        self.CancelLocationRequest = 0
        self.CancelLocationAnswer = 0
        self.NotifyRequest = 0
        self.NotifyAnswer = 0
        self.DeviceWatchdogRequest = 0
        self.DeviceWatchdogAnswer = 0
        self.Others = 0
        self.layer = 0

    def __str__(self):
        ret = "\nS6Counters: Layers = %s\n" % self.layer
        msg = ''
        if not self.full_display:
            if self.AuthenticationInformationRequest:
                msg += ("\t%35s = %s\n" % ('AuthenticationInformationRequest', self.AuthenticationInformationRequest))
            if self.AuthenticationInformationAnswer:
                msg += ("\t%35s = %s\n" % ('AuthenticationInformationAnswer', self.AuthenticationInformationAnswer))
            if self.UpdateLocationRequest:
                msg += ("\t%35s = %s\n" % ('UpdateLocationRequest', self.UpdateLocationRequest))
            if self.UpdateLocationAnswer:
                msg += ("\t%35s = %s\n" % ('UpdateLocationAnswer', self.UpdateLocationAnswer))
            if self.InsertSubscriberDataRequest:
                msg += ("\t%35s = %s\n" % ('InsertSubscriberDataRequest', self.InsertSubscriberDataRequest))
            if self.InsertSubscriberDataAnswer:
                msg += ("\t%35s = %s\n" % ('InsertSubscriberDataAnswer', self.InsertSubscriberDataAnswer))
            if self.PurgeUERequest:
                msg += ("\t%35s = %s\n" % ('PurgeUERequest', self.PurgeUERequest))
            if self.PurgeUEAnswer:
                msg += ("\t%35s = %s\n" % ('PurgeUEAnswer', self.PurgeUEAnswer))
            if self.IdentityCheckRequest:
                msg += ("\t%35s = %s\n" % ('IdentityCheckRequest', self.IdentityCheckRequest))
            if self.IdentityCheckAnswer:
                msg += ("\t%35s = %s\n" % ('IdentityCheckAnswer', self.IdentityCheckAnswer))
            if self.CancelLocationRequest:
                msg += ("\t%35s = %s\n" % ('CancelLocationRequest', self.CancelLocationRequest))
            if self.CancelLocationAnswer:
                msg += ("\t%35s = %s\n" % ('CancelLocationAnswer', self.CancelLocationAnswer))
            if self.NotifyRequest:
                msg += ("\t%35s = %s\n" % ('NotifyRequest', self.NotifyRequest))
            if self.NotifyAnswer:
                msg += ("\t%35s = %s\n" % ('NotifyAnswer', self.NotifyAnswer))
            if self.DeviceWatchdogRequest:
                msg += ("\t%35s = %s\n" % ('DeviceWatchdogRequest', self.DeviceWatchdogRequest))
            if self.DeviceWatchdogAnswer:
                msg += ("\t%35s = %s\n" % ('DeviceWatchdogAnswer', self.DeviceWatchdogAnswer))
            if self.Others:
                msg += ("\t%35s = %s\n" % ('Others', self.Others))

        return (ret + msg)

class GTPV2Counters:
    def __init__(self, displayAll = False):
        self.full_display = displayAll
        self.CreateSessionRequest = 0
        self.CreateSessionResponse = 0
        self.ModifyBearerRequest = 0
        self.ModifyBearerResponse = 0
        self.DeleteSessionRequest = 0
        self.DeleteSessionResponse = 0
        self.CreateBearerRequest = 0
        self.CreateBearerResponse = 0
        self.UpdateBearerRequest = 0
        self.UpdateBearerResponse = 0
        self.DeleteBearerRequest = 0
        self.DeleteBearerResponse = 0
        self.ReleaseAccessBearerRequest = 0
        self.ReleaseAccessBearerResponse = 0
        self.ForwardRelocationRequest = 0
        self.ForwardRelocationResponse = 0
        self.ForwardRelocationCompleteNotification = 0
        self.ForwardRelocationCompleteAcknowledge = 0
        self.ContextRequest = 0
        self.ContextResponse = 0
        self.ContextAcknowledge = 0
        self.ForwardAccessCompleteNotification = 0
        self.ForwardAccessCompleteAcknowledge = 0
        self.RelocationCancelRequest = 0
        self.RelocationCancelResponse = 0
        self.ReleaseAccessBearersRequest = 0
        self.ReleaseAccessBearersResponse = 0
        self.DownlinkDataNotification = 0
        self.DownlinkDataNotificationAcknowledgement = 0
        self.DownlinkDataNotificationFailureIndication = 0

        self.Others = 0
        self.layer = 0

    def __str__(self):
        ret = "\nGTPV2Counters: Layers = %s\n" % self.layer
        msg = ''
        if not self.full_display:
            if self.CreateSessionRequest:
                msg += ("\t%35s = %s\n" % ('CreateSessionRequest', self.CreateSessionRequest))
            if self.CreateSessionResponse:
                msg += ("\t%35s = %s\n" % ('CreateSessionResponse', self.CreateSessionResponse))
            if self.ModifyBearerRequest:
                msg += ("\t%35s = %s\n" % ('ModifyBearerRequest', self.ModifyBearerRequest))
            if self.ModifyBearerResponse:
                msg += ("\t%35s = %s\n" % ('ModifyBearerResponse', self.ModifyBearerResponse))
            if self.DeleteSessionRequest:
                msg += ("\t%35s = %s\n" % ('DeleteSessionRequest', self.DeleteSessionRequest))
            if self.DeleteSessionResponse:
                msg += ("\t%35s = %s\n" % ('DeleteSessionResponse', self.DeleteSessionResponse))
            if self.CreateBearerRequest:
                msg += ("\t%35s = %s\n" % ('CreateBearerRequest', self.CreateBearerRequest))
            if self.CreateBearerResponse:
                msg += ("\t%35s = %s\n" % ('CreateBearerResponse', self.CreateBearerResponse))
            if self.UpdateBearerRequest:
                msg += ("\t%35s = %s\n" % ('UpdateBearerRequest', self.UpdateBearerRequest))
            if self.UpdateBearerResponse:
                msg += ("\t%35s = %s\n" % ('UpdateBearerResponse', self.UpdateBearerResponse))
            if self.DeleteBearerRequest:
                msg += ("\t%35s = %s\n" % ('DeleteBearerRequest', self.DeleteBearerRequest))
            if self.DeleteBearerResponse:
                msg += ("\t%35s = %s\n" % ('DeleteBearerResponse', self.DeleteBearerResponse))
            if self.ReleaseAccessBearerRequest:
                msg += ("\t%35s = %s\n" % ('ReleaseAccessBearerRequest', self.ReleaseAccessBearerRequest))
            if self.ReleaseAccessBearerResponse:
                msg += ("\t%35s = %s\n" % ('ReleaseAccessBearerResponse', self.ReleaseAccessBearerResponse))
            if self.ForwardRelocationRequest:
                msg += ("\t%35s = %s\n" % ('ForwardRelocationRequest', self.ForwardRelocationRequest))
            if self.ForwardRelocationResponse:
                msg += ("\t%35s = %s\n" % ('ForwardRelocationResponse', self.ForwardRelocationResponse))
            if self.ForwardRelocationCompleteNotification:
                msg += ("\t%35s = %s\n" % ('ForwardRelocationCompleteNotification', self.ForwardRelocationCompleteNotification))
            if self.ForwardRelocationCompleteAcknowledge:
                msg += ("\t%35s = %s\n" % ('ForwardRelocationCompleteAcknowledge', self.ForwardRelocationCompleteAcknowledge))
            if self.ContextRequest:
                msg += ("\t%35s = %s\n" % ('ContextRequest', self.ContextRequest))
            if self.ContextResponse:
                msg += ("\t%35s = %s\n" % ('ContextResponse', self.ContextResponse))
            if self.ContextAcknowledge:
                msg += ("\t%35s = %s\n" % ('ContextAcknowledge', self.ContextAcknowledge))
            if self.ForwardAccessCompleteNotification:
                msg += ("\t%35s = %s\n" % ('ForwardAccessCompleteNotification', self.ForwardAccessCompleteNotification))
            if self.ForwardAccessCompleteAcknowledge:
                msg += ("\t%35s = %s\n" % ('ForwardAccessCompleteAcknowledge', self.ForwardAccessCompleteAcknowledge))
            if self.RelocationCancelRequest:
                msg += ("\t%35s = %s\n" % ('RelocationCancelRequest', self.RelocationCancelRequest))
            if self.RelocationCancelResponse:
                msg += ("\t%35s = %s\n" % ('RelocationCancelResponse', self.RelocationCancelResponse))
            if self.ReleaseAccessBearersRequest:
                msg += ("\t%35s = %s\n" % ('ReleaseAccessBearersRequest', self.ReleaseAccessBearersRequest))
            if self.ReleaseAccessBearersResponse:
                msg += ("\t%35s = %s\n" % ('ReleaseAccessBearersResponse', self.ReleaseAccessBearersResponse))
            if self.DownlinkDataNotification:
                msg += ("\t%35s = %s\n" % ('DownlinkDataNotification', self.DownlinkDataNotification))
            if self.DownlinkDataNotificationAcknowledgement:
                msg += ("\t%35s = %s\n" % ('DownlinkDataNotificationAcknowledgement', self.DownlinkDataNotificationAcknowledgement))
            if self.DownlinkDataNotificationFailureIndication:
                msg += ("\t%35s = %s\n" % ('DownlinkDataNotificationFailureIndication', self.DownlinkDataNotificationFailureIndication))

            if self.Others:
                msg += ("\t%35s = %s\n" % ('Others', self.Others))

        return ret + msg

class MainCollectorCounters:
    def __init__(self, cntlock, displayAll = False):
        self.cntlock = cntlock
        self.s1_counters = S1Counters(displayAll)
        self.s6_counters = S6Counters(displayAll)
        self.gtpv2_counters = GTPV2Counters(displayAll)
        self.pkt_cnt = 0

    def packable_get(self):
        return [self.s1_counters, self.s6_counters]

    def __str__(self):
        pc = "\nPackets = %s" % self.pkt_cnt
        s1 = self.s1_counters
        s6 = self.s6_counters
        gtpv2 = self.gtpv2_counters
        return pc + str(s1) + str(s6) + str(gtpv2)

    def merge_pkt_cnt(self, pkt_cnt):
        self.cntlock.acquire()
        self.pkt_cnt += pkt_cnt
        self.cntlock.release()

    def merge(self, s1, s6, gtpv2):
        self.merge(s1)
        self.merge(s6)
        self.merge(gtpv2)

    def merge_s1(self, rs1):
        self.cntlock.acquire()
        s1 = self.s1_counters
        s1.HandoverRequestAcknowledge += rs1.HandoverRequestAcknowledge
        s1.HandoverRequest += rs1.HandoverRequest
        s1.HandoverCancel += rs1.HandoverCancel
        s1.HandoverRequired += rs1.HandoverRequired
        s1.HandoverPreparationFailure += rs1.HandoverPreparationFailure
        s1.HandoverCancelAcknowledge += rs1.HandoverCancelAcknowledge
        s1.HandoverNotify += rs1.HandoverNotify
        s1.HandoverFailure += rs1.HandoverFailure
        s1.HandoverCommand += rs1.HandoverCommand

        s1.UEContextReleaseComplete += rs1.UEContextReleaseComplete
        s1.UEContextReleaseCommand += rs1.UEContextReleaseCommand
        s1.UEContextReleaseRequest += rs1.UEContextReleaseRequest
        s1.PathSwitchRequestFailure += rs1.PathSwitchRequestFailure
        s1.PathSwitchRequest += rs1.PathSwitchRequest
        s1.PathSwitchRequestAcknowledge += rs1.PathSwitchRequestAcknowledge
        s1.DownlinkNASTransport += rs1.DownlinkNASTransport
        s1.UplinkNASTransport += rs1.UplinkNASTransport

        s1.NASNonDeliveryIndication += rs1.NASNonDeliveryIndication
        s1.InitialUEMessage += rs1.InitialUEMessage
        s1.InitialContextSetupRequest += rs1.InitialContextSetupRequest
        s1.InitialContextSetupResponse += rs1.InitialContextSetupResponse
        s1.E_RABReleaseCommand += rs1.E_RABReleaseCommand
        s1.E_RABModifyRequest += rs1.E_RABModifyRequest
        s1.E_RABSetupRequest += rs1.E_RABSetupRequest
        s1.E_RABSetupResponse += rs1.E_RABSetupResponse

        s1.UECapabilityInfoIndication += rs1.UECapabilityInfoIndication

        s1.Others += rs1.Others
        s1.Paging += rs1.Paging
        s1.layer += rs1.layer

        self.cntlock.release()


    def merge_s6(self, rs6):
        self.cntlock.acquire()

        s6 = self.s6_counters
        s6.PurgeUEAnswer += rs6.PurgeUEAnswer
        s6.PurgeUERequest += rs6.PurgeUERequest
        s6.InsertSubscriberDataAnswer += rs6.InsertSubscriberDataAnswer
        s6.InsertSubscriberDataRequest += rs6.InsertSubscriberDataRequest
        s6.AuthenticationInformationRequest += rs6.AuthenticationInformationRequest
        s6.AuthenticationInformationAnswer += rs6.AuthenticationInformationAnswer
        s6.UpdateLocationRequest += rs6.UpdateLocationRequest
        s6.UpdateLocationAnswer += rs6.UpdateLocationAnswer
        s6.IdentityCheckRequest += rs6.IdentityCheckRequest
        s6.IdentityCheckAnswer += rs6.IdentityCheckAnswer
        s6.CancelLocationRequest += rs6.CancelLocationRequest
        s6.CancelLocationAnswer += rs6.CancelLocationAnswer
        s6.NotifyRequest += rs6.NotifyRequest
        s6.NotifyAnswer += rs6.NotifyAnswer
        s6.DeviceWatchdogRequest += rs6.DeviceWatchdogRequest
        s6.DeviceWatchdogAnswer += rs6.DeviceWatchdogAnswer
        s6.Others += rs6.Others


        s6.layer += rs6.layer

        self.cntlock.release()

    def merge_gtpv2(self, rgtpv2):
        self.cntlock.acquire()

        gtpv2 = self.gtpv2_counters
        gtpv2.CreateSessionRequest += rgtpv2.CreateSessionRequest
        gtpv2.CreateSessionResponse += rgtpv2.CreateSessionResponse
        gtpv2.ModifyBearerRequest += rgtpv2.ModifyBearerRequest
        gtpv2.ModifyBearerResponse += rgtpv2.ModifyBearerResponse
        gtpv2.DeleteSessionRequest += rgtpv2.DeleteSessionRequest
        gtpv2.DeleteSessionResponse += rgtpv2.DeleteSessionResponse
        gtpv2.CreateBearerRequest += rgtpv2.CreateBearerRequest
        gtpv2.CreateBearerResponse += rgtpv2.CreateBearerResponse
        gtpv2.UpdateBearerRequest += rgtpv2.UpdateBearerRequest
        gtpv2.UpdateBearerResponse += rgtpv2.UpdateBearerResponse
        gtpv2.DeleteBearerRequest += rgtpv2.DeleteBearerRequest
        gtpv2.DeleteBearerResponse += rgtpv2.DeleteBearerResponse
        gtpv2.ReleaseAccessBearerRequest += rgtpv2.ReleaseAccessBearerRequest
        gtpv2.ReleaseAccessBearerResponse += rgtpv2.ReleaseAccessBearerResponse
        gtpv2.ForwardRelocationRequest += rgtpv2.ForwardRelocationRequest
        gtpv2.ForwardRelocationResponse += rgtpv2.ForwardRelocationResponse
        gtpv2.ForwardRelocationCompleteNotification += rgtpv2.ForwardRelocationCompleteNotification
        gtpv2.ForwardRelocationCompleteAcknowledge += rgtpv2.ForwardRelocationCompleteAcknowledge
        gtpv2.ContextRequest += rgtpv2.ContextRequest
        gtpv2.ContextResponse += rgtpv2.ContextResponse
        gtpv2.ContextAcknowledge += rgtpv2.ContextAcknowledge
        gtpv2.ForwardAccessCompleteNotification += rgtpv2.ForwardAccessCompleteNotification
        gtpv2.ForwardAccessCompleteAcknowledge += rgtpv2.ForwardAccessCompleteAcknowledge
        gtpv2.RelocationCancelRequest += rgtpv2.RelocationCancelRequest
        gtpv2.RelocationCancelResponse += rgtpv2.RelocationCancelResponse
        gtpv2.ReleaseAccessBearersRequest += rgtpv2.ReleaseAccessBearersRequest
        gtpv2.ReleaseAccessBearersResponse += rgtpv2.ReleaseAccessBearersResponse
        gtpv2.DownlinkDataNotification += rgtpv2.DownlinkDataNotification
        gtpv2.DownlinkDataNotificationAcknowledgement = rgtpv2.DownlinkDataNotificationAcknowledgement
        gtpv2.DownlinkDataNotificationFailureIndication += rgtpv2.DownlinkDataNotificationFailureIndication

        gtpv2.Others += rgtpv2.Others

        gtpv2.layer += rgtpv2.layer

        self.cntlock.release()
