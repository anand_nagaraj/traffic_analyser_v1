import logging
import pickle

from ExtractFields import *
from IpLayer import *
from Counters import S1Counters


globalS1APDetails = {}

logger = logging.getLogger('')

class S1APDetails:
    def __init__(self):
        self.enbid = []
        self.mmeid = []
        self.enbip = []
        self.mmeip = []
        self.s1apType = []
        self.tmsi = []
        self.guti = []
        self.imsi = []
        self.s1apid = []
        self.naspdu = []
        self.cellid = []
        self.dst_cellid = []
        self.res = []
        self.erabid = []
        self.tac = []
        self.transport_layer_addr_ipv4 = []
        self.mcc = []
        self.mnc = []
        self.hotype = []
        self.gtpTEID = []

    def __str__(self):
        return ("enbid=%s mmeid=%s enbip=%s mmeip=%s s1apType=%s tmsi=%s imsi=%s guti=%s cellid=%s res=%s, naspdu=%s s1apid=%s"
                %(self.enbid, self.mmeid, self.enbip, self.mmeip, self.s1apType, self.tmsi, self.imsi, self.guti,
                  self.cellid, self.res, self.naspdu, self.s1apid))

    def condition_check_field(self, newx, listx):
        return len(newx) and newx[0] not in listx

    def merge(self, new):
        if self.condition_check_field(new.enbid, self.enbid):
            self.enbid += new.enbid
        if self.condition_check_field(new.mmeid, self.mmeid):
            self.mmeid += new.mmeid
        if self.condition_check_field(new.enbip, self.enbip):
            self.enbip += new.enbip
        if self.condition_check_field(new.mmeip, self.mmeip):
            self.mmeip += new.mmeip
        if self.condition_check_field(new.tmsi, self.tmsi):
            self.tmsi += new.tmsi
        if self.condition_check_field(new.imsi, self.imsi):
            self.imsi += new.imsi
        if self.condition_check_field(new.guti, self.guti):
            self.guti += new.guti
        if self.condition_check_field(new.cellid, self.cellid):
            self.cellid += new.cellid
        if self.condition_check_field(new.res, self.res):
            self.res += new.res
        if self.condition_check_field(new.s1apid, self.s1apid):
            self.s1apid += new.s1apid
        if self.condition_check_field(new.naspdu, self.naspdu):
            self.naspdu += new.naspdu

        return self


class S1APProcessor:
    def __init__(self, sqlOperation, mainCollector, collectorCarrier, mmeip):
        self.num_of_layers = 0
        self.sql = sqlOperation
        self.mmeip = mmeip
        self.counters = S1Counters(False)
        self.mainCollector = mainCollector
        self.collectorCarrier = collectorCarrier
        self.cur_pkt_cnt = 0
        self.old_pkt_cnt = 0

    def print_global_details(self):
        logger.debug('\n\nGlobalS1APDetails S1AP Only:\n')
        for key, value in globalS1APDetails.items():
            logger.debug('[enbid=%s : %s]' % (key, value))


    def process_sctp_layer(self, packet):
        sctp = packet['sctp']

        if sctp != None:
            print('SCTP Layer:')
            if isFieldExtractable(sctp, 'data_payload_proto_id') is not None:
                if sctp.data_payload_proto_id == 18:
                    # Either process it here or process it next layer anyways.
                    self.process_s1ap_layer(packet)

    def insert_s1ap_details(self, s1apDetails, pkt_count):
        # FIXME: Currently working only with enbid, should bring upon mmeid logic too.
        if len(s1apDetails.enbid) <= 0:
            return

        key = s1apDetails.enbid[0]
        if not globalS1APDetails.has_key(key):
            globalS1APDetails[key] = s1apDetails
        else:
            lhs = globalS1APDetails[key]
            globalS1APDetails[key] = lhs.merge(s1apDetails)

        logger.debug ("pkt=%s %s" %(pkt_count, globalS1APDetails[key]))


    def update_counter(self, value, field_names):
        s1counters = self.counters
        s1counters.layer += 1
        if 'id-initialUEMessage' == value:
            s1counters.InitialUEMessage += 1
        elif 'id-InitialContextSetup' == value:
            initial_ctx_flag = True
            for elem in field_names:
                if 'initialcontextsetuprequest' == elem:
                    s1counters.InitialContextSetupRequest += 1
                    initial_ctx_flag = False
                elif 'initialcontextsetupresponse' == elem:
                    s1counters.InitialContextSetupResponse += 1
                    initial_ctx_flag = False
            if initial_ctx_flag:
                s1counters.Others += 1
        elif 'id-downlinkNASTransport' == value:
            s1counters.DownlinkNASTransport += 1
        elif 'id-uplinkNASTransport' == value:
            s1counters.UplinkNASTransport += 1
        elif 'id-E-RABSetup' == value:
            e_rab_flag = True
            for elem in field_names:
                if 'e_rabsetuprequest' == elem:
                    s1counters.E_RABSetupRequest += 1
                    e_rab_flag = False
                elif 'e_rabmodifyequest' == elem:
                    s1counters.E_RABModifyRequest += 1
                    e_rab_flag = False
                elif 'e_rabreleasecommand' == elem:
                    s1counters.E_RABReleaseCommand  += 1
                    e_rab_flag = False
                elif 'e_rabsetupresponse' == elem:
                    s1counters.E_RABSetupResponse += 1
                    e_rab_flag = False
            if e_rab_flag:
                s1counters.Others += 1
        elif 'id-HandoverPreparation' == value:
            #logger.info(s1ap)
            # for f in field_names:
            #     if 'hand' in f:
            #         logger.info(f)
            # FIXME: Don't loop through like this, do it in one loop.
            handover_flag = True
            for elem in field_names:
                if 'handoverrequired' == elem:
                    s1counters.HandoverRequired += 1
                    handover_flag = False
                elif 'handoverfailure' == elem:
                    s1counters.HandoverFailure += 1
                    handover_flag = False
                elif 'handovernotify' == elem:
                    s1counters.HandoverNotify += 1
                    handover_flag = False
                elif 'handovercancel' == elem:
                    s1counters.HandoverCancel += 1
                    handover_flag = False
                elif 'handovercancelacknowledge' == elem:
                    s1counters.HandoverCancelAcknowledge += 1
                    handover_flag = False
                elif 'handoverrequest' == elem:
                    s1counters.HandoverRequest += 1
                    handover_flag = False
                elif 'handoverrequestacknowledge' == elem:
                    s1counters.HandoverRequestAcknowledge += 1
                    handover_flag = False
                elif 'handoverrequired' == elem:
                    s1counters.HandoverRequired += 1
                    handover_flag = False
                elif 'handovercommand' == elem:
                    s1counters.HandoverCommand += 1
                    handover_flag = False
            if handover_flag:
                s1counters.Others += 1
        elif 'id-nasNonDeliveryIndication' == value:
            s1counters.NASNonDeliveryIndication += 1
        elif 'id-PathSwitchRequest' == value:
            s1counters.PathSwitchRequest += 1
        elif 'id-PathSwitchRequestAcknowledge' == value:
            s1counters.PathSwitchRequestAcknowledge += 1
        elif 'id-PathSwitchRequestFailure' == value:
            s1counters.PathSwitchRequestFailure += 1
        elif 'id-UEContextRelease' == value:
            ue_ctx_flag = True
            for elem in field_names:
                if 'uecontextreleasecommand' == elem:
                    s1counters.UEContextReleaseCommand += 1
                    ue_ctx_flag = False
                elif 'uecontextreleasecomplete' == elem:
                    s1counters.UEContextReleaseComplete += 1
                    ue_ctx_flag = False
            if ue_ctx_flag:
                s1counters.Others += 1
        elif 'id-UEContextReleaseRequest' == value:
            s1counters.UEContextReleaseRequest += 1
        elif 'id-Paging' == value:
            s1counters.Paging += 1
        elif 'id-UECapabilityInfoIndication' == value:
            s1counters.UECapabilityInfoIndication +=1
        else:
            #logger.info('Not accounting:%s' % value)
            s1counters.Others += 1


    def process_s1ap_layer(self, pkt_count, pkt_time, src, dst, fields, field_names, fname):

        self.num_of_layers += 1
        self.cur_pkt_cnt = pkt_count
        if self.old_pkt_cnt > self.cur_pkt_cnt:
            self.old_pkt_cnt = 0  # Time to reset it.

        # FIXME: Now, which is enbip or mmeip is hard to decide unless we decode the msg type inside NASPDU
        if len(fields):
            s1apDetails = S1APDetails()

            if src in self.mmeip:
                s1apDetails.mmeip = src
                s1apDetails.enbip = dst
                uplink = 0
            else:
                s1apDetails.mmeip = dst
                s1apDetails.enbip = src
                uplink = 1

            if self.mmeip is not None:
                if s1apDetails.mmeip not in self.mmeip:
                    return

            # Now try to get any fields from here.
            for key,value in fields.items():
                cell_flag = None
                # if 'id' == key:
                #     if type(value) is not str:
                #         s1apDetails.s1apid += value
                #     else:
                #         s1apDetails.s1apid.append(value)
                if 'IMSI' == key:
                    if type(value) is not str:
                        s1apDetails.imsi += value
                    else:
                        s1apDetails.imsi.append(value)
                elif 'RES' == key:
                    if type(value) is not str:
                        s1apDetails.res += value
                    else:
                        s1apDetails.res.append(value)
                elif ('ENB-UE-S1AP-ID' == key) or ('eNB-UE-S1AP-ID' == key):
                    if type(value) is not str:
                        s1apDetails.enbid += value
                    else:
                        s1apDetails.enbid.append(value)
                elif ('MME-UE-S1AP-ID' == key) or ('mME-UE-S1AP-ID' == key):
                    if type(value) is not str:
                        s1apDetails.mmeid += value
                    else:
                        s1apDetails.mmeid.append(value)
                elif 'procedureCode' == key:
                    if type(value) is not str:
                        self.update_counter(value[0], field_names)
                        cmd = value[0]
                    else:
                        self.update_counter(value, field_names)
                        cmd = value
                # elif 'e-RAB-ID' == key:
                #     if type(value) is not str:
                #         s1apDetails.erabid += value
                #     else:
                #         s1apDetails.erabid.append(value)
                # elif 'tAC' == key:
                #     if type(value) is not str:
                #         s1apDetails.tac += int(value,16)
                #     else:
                #         tac_val = str(int(value,16))
                #         s1apDetails.tac.append(tac_val)
                # elif 'transportLayerAddress(IPv4)' == key:
                #     if type(value) is not str:
                #         s1apDetails.transport_layer_addr_ipv4 += value
                #     else:
                #         s1apDetails.transport_layer_addr_ipv4.append(value)
                # elif 'mcc' == key:
                #     if type(value) is not str:
                #         s1apDetails.mcc += value
                #     else:
                #         s1apDetails.mcc.append(value)
                # elif 'mnc' == key:
                #     if type(value) is not str:
                #         s1apDetails.mnc += value
                #     else:
                #         s1apDetails.mnc.append(value)
                # elif 'HandoverType' == key:
                #     val = str(value).lower()
                #     if val == 'intralte':
                #         hoval = '0'
                #     elif val == 'ltetoutran':
                #         hoval += '1'
                #     elif val == 'ltetogeran':
                #         hoval = '3'
                #     elif val == 'utrantolte':
                #         hoval = '4'
                #     elif val == 'gerantolte':
                #         hoval = '5'
                #     s1apDetails.hotype += hoval
                # elif 'cell-ID' == key and cell_flag == 'target_cell_id':
                #     if type(value) is not str:
                #         s1apDetails.dst_cellid += value
                #     else:
                #         s1apDetails.dst_cellid.append(value)
                #     cell_flag = None
                # elif 'gTP-TEID' == key:
                #     if type(value) is not str:
                #         s1apDetails.gtpTEID += value
                #     else:
                #         s1apDetails.gtpTEID.append(value)

                elif 'Source-ToTarget-TransparentContainer' == key:
                    cell_flag = 'target_cell_id'
                elif 'cell-ID' == key:
                    if type(value) is not str:
                        s1apDetails.cellid += value
                    else:
                        s1apDetails.cellid.append(value)
                elif 'macroENB-ID' == key:
                    #TODO should retrieve 2nd value of list to get for both source and destination enbid, currently it's target enbid
                    # logger.info('key->value %s %s' % (key,value))
                    # if type(value) is not str:
                    #     s1apDetails.enbid += value
                    # else:
                    #     s1apDetails.enbid.append(value)
                    pass

            # print s1apDetails
            # self.insert_s1ap_details(s1apDetails, pkt_count)

            valid = False

            #FIXME: This is dirty filling, may be try filling it via indices
            s1dbfields = [s1apDetails.enbip, s1apDetails.mmeip]
            if len(s1apDetails.enbid):
                s1dbfields.append(s1apDetails.enbid[0])
                valid = True
            else:
                s1dbfields.append(0)
            if len(s1apDetails.mmeid):
                s1dbfields.append(s1apDetails.mmeid[0])
                valid = True
            else:
                s1dbfields.append(0)

            s1dbfields.append((s1apDetails.s1apType[0]) if len(s1apDetails.s1apType) else 0)
            s1dbfields.append((s1apDetails.tmsi[0]) if len(s1apDetails.tmsi) else '')
            s1dbfields.append((s1apDetails.imsi[0]) if len(s1apDetails.imsi) else '')
            s1dbfields.append((s1apDetails.guti[0]) if len(s1apDetails.guti) else '')
            s1dbfields.append((s1apDetails.cellid[0]) if len(s1apDetails.cellid) else '')
            s1dbfields.append((s1apDetails.res[0]) if len(s1apDetails.res) else '')
            # s1dbfields.append((s1apDetails.naspdu[0]) if len(s1apDetails.naspdu) else '')
            # s1dbfields.append((s1apDetails.s1apid[0]) if len(s1apDetails.s1apid) else 0)
            # s1dbfields.append((s1apDetails.erabid[0]) if len(s1apDetails.erabid) else 0)
            # s1dbfields.append((s1apDetails.tac[0]) if len(s1apDetails.tac) else 0)
            # s1dbfields.append((s1apDetails.transport_layer_addr_ipv4[0]) if len(s1apDetails.transport_layer_addr_ipv4) else 0)
            # s1dbfields.append((s1apDetails.mnc[0]) if len(s1apDetails.mnc) else 0)
            # s1dbfields.append((s1apDetails.mcc[0]) if len(s1apDetails.mcc) else 0)
            # s1dbfields.append((s1apDetails.hotype[0]) if len(s1apDetails.hotype) else 0)
            # s1dbfields.append((s1apDetails.dst_cellid[0]) if len(s1apDetails.dst_cellid) else '')
            # s1dbfields.append((s1apDetails.gtpTEID[0]) if len(s1apDetails.gtpTEID) else '')

            callFlowdb = []
            callFlowdb.append(pkt_count)
            callFlowdb.append(pkt_time)
            callFlowdb.append(fname)
            callFlowdb.append(s1dbfields[0])
            callFlowdb.append(s1dbfields[1])
            callFlowdb.append(s1dbfields[2])
            callFlowdb.append(s1dbfields[3])
            callFlowdb.append(cmd)
            callFlowdb.append(uplink)

            # logger.warn('Cannot push this pkt %s from SCTP to DB, So skipping' % pkt_count)
            # return  # THis is to return without pushing anything to DB

            if valid:
                try:
                    self.sql.Insert_or_Update("S1", s1dbfields)
                    self.sql.Insert("CALLFLOW_S1", callFlowdb)
                except Exception as e:
                    print (e)

    def get_counters(self):
        return self.num_of_layers

    def print_counters(self):
        logger.info(self.counters)

    def packable_get(self):
        tup = ("S1", self.counters, None)
        return tup

    def push_counters(self):
        serialized_data = self.mainCollector.pickle_pack(self.packable_get())
        self.collectorCarrier.put(serialized_data)
        self.reset_counters()

    def reset_counters(self, displayAll = False):
        self.counters.InitialUEMessage = 0
        self.counters.InitialContextSetupRequest = 0
        self.counters.InitialContextSetupResponse = 0
        self.counters.DownlinkNASTransport = 0
        self.counters.E_RABModifyRequest = 0
        self.counters.E_RABReleaseCommand = 0
        self.counters.E_RABSetupRequest = 0
        self.counters.E_RABSetupResponse = 0
        self.counters.HandoverCommand = 0
        self.counters.HandoverFailure = 0
        self.counters.HandoverNotify = 0
        self.counters.HandoverCancel = 0
        self.counters.HandoverCancelAcknowledge = 0
        self.counters.HandoverPreparationFailure = 0
        self.counters.HandoverRequest = 0
        self.counters.HandoverRequestAcknowledge = 0
        self.counters.HandoverRequired = 0
        self.counters.NASNonDeliveryIndication = 0
        self.counters.PathSwitchRequest = 0
        self.counters.PathSwitchRequestAcknowledge = 0
        self.counters.PathSwitchRequestFailure = 0
        self.counters.UEContextReleaseRequest = 0
        self.counters.UEContextReleaseCommand = 0
        self.counters.UEContextReleaseComplete = 0
        self.counters.UplinkNASTransport = 0
        self.counters.Paging = 0
        self.counters.UECapabilityInfoIndication = 0
        self.counters.Others = 0
        self.counters.full_display = displayAll
        self.counters.layer = 0
