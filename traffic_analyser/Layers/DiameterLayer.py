import logging
from ExtractFields import *
from Counters import S6Counters

from IpLayer import *

globalAuthDetails = {}

logger = logging.getLogger()

class S6AuthDetails:
    def __init__(self):
        self.hopbyhopid = [] #This is acting as key for dictionary
        self.endtoendid = []
        self.xres = []
        self.kasme = []
        self.imsi = []
        self.imei = []
        self.msisdn = []
        self.rat_type = []
        self.mmeip = []
        self.hssip = []

    def __str__(self):
        return ('S6Data hopbyhop=%s endtoend=%s rat=%s imsi=%s imei=%s msisdn=%s xres=%s kasme=%s'
                %(self.hopbyhopid, self.endtoendid, self.rat_type, self.imsi, self.imei, self.msisdn,
                  self.xres, self.kasme))

    def condition_check_field(self, newx, listx):
        return len(newx) and newx[0] not in listx

    def merge(self, new):
        if self.condition_check_field(new.xres, self.xres):
            self.xres += new.xres #Concatenate to this list
        if self.condition_check_field(new.imsi, self.imsi):
            self.imsi += new.imsi
        if self.condition_check_field(new.imei, self.imei):
            self.imei += new.imei
        if self.condition_check_field(new.msisdn, self.msisdn):
            self.msisdn += new.msisdn
        if self.condition_check_field(new.kasme, self.kasme):
            self.kasme += new.kasme  # Concatenate to this list
        if new.endtoendid is not None:
            self.endtoendid = new.endtoendid
        if new.hopbyhopid is not None:
            self.hopbyhopid = new.hopbyhopid
        if new.rat_type is not None:
            self.rat_type = new.rat_type

        return self


class S6Processor:
    def __init__(self, sqlOperation, mainCollector, collectorCarrier, mmeip):
        self.num_of_messages = 0
        self.mmeip = mmeip
        self.sql = sqlOperation
        self.counters = S6Counters(False)
        self.mainCollector = mainCollector
        self.collectorCarrier = collectorCarrier
        self.old_pkt_cnt = 0
        self.cur_pkt_cnt = 0

    def print_global_details(self):
        logger.debug('GlobalAuthDetails S6a Only:\n')
        for key,value in globalAuthDetails.items():
            logger.debug('[hopid=%s : %s]' %(key, value))


    def insert_details(self, s6AuthDetails, pkt_count):
        key = s6AuthDetails.hopbyhopid[0]
        if not globalAuthDetails.has_key(key):
            globalAuthDetails[key] = s6AuthDetails
        else:
            lhs = globalAuthDetails[key]
            globalAuthDetails[key] = lhs.merge(s6AuthDetails)

        logger.debug ("pkt=%s %s" % (pkt_count, globalAuthDetails[key]))

    def update_counter(self, value, req):
        counters = self.counters
        counters.layer += 1
        cmd = ''
        if value == 318:
            if req == 1:
                counters.AuthenticationInformationRequest += 1
                cmd = 'AuthenticationInformationRequest'
            else:
                counters.AuthenticationInformationAnswer += 1
                cmd = 'AuthenticationInformationAnswer'
        elif value == 316:
            if req == 1:
                counters.UpdateLocationRequest += 1
                cmd = 'UpdateLocationRequest'
            else:
                counters.UpdateLocationAnswer += 1
                cmd = 'UpdateLocationAnswer'
        elif value == 319:
            if req == 1:
                counters.InsertSubscriberDataRequest += 1
                cmd = 'InsertSubscriberDataRequest'
            else:
                counters.InsertSubscriberDataAnswer += 1
                cmd = 'InsertSubscriberDataAnswer'
        elif value == 321:
            if req == 1:
                counters.PurgeUERequest += 1
                cmd = 'PurgeUERequest'
            else:
                counters.PurgeUEAnswer += 1
                cmd = 'PurgeUEAnswer'
        elif value == 324:
            if req == 1:
                counters.IdentityCheckRequest += 1
                cmd = 'IdentityCheckRequest'
            else:
                counters.IdentityCheckAnswer += 1
                cmd = 'IdentityCheckAnswer'
        elif value == 317:
            if req == 1:
                counters.CancelLocationRequest += 1
                cmd = 'CancelLocationRequest'
            else:
                counters.CancelLocationAnswer += 1
                cmd = 'CancelLocationAnswer'
        elif value == 323:
            if req == 1:
                counters.NotifyRequest += 1
                cmd = 'NotifyRequest'
            else:
                counters.NotifyAnswer += 1
                cmd = 'NotifyAnswer'
        elif value == 280:
            if req == 1:
                counters.DeviceWatchdogRequest += 1
                cmd = 'DeviceWatchdogRequest'
            else:
                counters.DeviceWatchdogAnswer += 1
                cmd = 'DeviceWatchdogAnswer'
        else:
            counters.Others += 1

        return cmd


    def parse_diameter_layer(self, pkt_count, pkt_time, src, dst, fields, req, fname):
        self.num_of_messages += 1
        self.cur_pkt_cnt = pkt_count
        if self.old_pkt_cnt > self.cur_pkt_cnt:
            self.old_pkt_cnt = 0 # Time to reset it.

        if len(fields):

            s6AuthDetails = S6AuthDetails()

            if src in self.mmeip:
                s6AuthDetails.mmeip = src
                s6AuthDetails.hssip = dst
            else:
                s6AuthDetails.mmeip = dst
                s6AuthDetails.hssip = src

            if self.mmeip is not None:
                if s6AuthDetails.mmeip not in self.mmeip:
                    return

            # Now try to get any fields from here.
            for key,value in fields.items():
                if 'Hop-by-Hop' in key:
                    if type(value) is not str:
                        s6AuthDetails.hopbyhopid = value
                    else:
                        s6AuthDetails.hopbyhopid.append(value)
                elif 'End-to-End' in key:
                    if type(value) is not str:
                        s6AuthDetails.endtoendid = value
                    else:
                        s6AuthDetails.endtoendid.append(value)
                elif 'User-Name' in key:
                    if type(value) is not str:
                        s6AuthDetails.imsi = value
                    else:
                        s6AuthDetails.imsi.append(value)
                elif 'IMEI' in key:
                    if type(value) is not str:
                        s6AuthDetails.imei = value
                    else:
                        s6AuthDetails.imei.append(value)
                elif 'MSISDN' in key:
                    if 'Not' not in value:
                        if type(value) is not str:
                            s6AuthDetails.msisdn = value
                        else:
                            s6AuthDetails.msisdn.append(value)
                elif 'KASME' in key:
                    if type(value) is not str:
                        s6AuthDetails.kasme = value
                    else:
                        s6AuthDetails.kasme.append(value)
                elif 'XRES' in key:
                    if type(value) is not str:
                        s6AuthDetails.xres = value
                    else:
                        s6AuthDetails.xres.append(value)
                elif 'RAT-Type' in key:
                    if type(value) is not str:
                        s6AuthDetails.rat_type = value
                    else:
                        s6AuthDetails.rat_type.append(value)
                elif 'Command Code' in key:
                    dir = 1 if int(req) else 0
                    if type(value) is not str:
                        cmd = self.update_counter(int(value[0]), dir)
                    else:
                        cmd = self.update_counter(int(value), dir)
            # self.insert_details(s6AuthDetails, pkt_count)

            # print s6AuthDetails

            # logger.warn('Cannot push this pkt %s from S6 to DB, So skipping' % pkt_count)
            # return # THis is to return without pushing anything to DB

            try :
                callFlowdb = []
                callFlowdb.append(pkt_count)
                callFlowdb.append(pkt_time)
                callFlowdb.append(fname)
                callFlowdb.append(s6AuthDetails.mmeip)
                callFlowdb.append(s6AuthDetails.hssip)
                callFlowdb.append(s6AuthDetails.hopbyhopid[0]
                                  if type(s6AuthDetails.hopbyhopid) is not str else s6AuthDetails.hopbyhopid)
                callFlowdb.append(s6AuthDetails.endtoendid[0]
                                  if type(s6AuthDetails.endtoendid) is not str else s6AuthDetails.endtoendid)
                callFlowdb.append(cmd)
                callFlowdb.append(dir)

                # Final insertion to callflow table
                self.sql.Insert("CALLFLOW_S6", callFlowdb)
            except Exception as e:
                print e

            try:
                all_xres = ''
                all_kasme = ''
                s6Fields = [s6AuthDetails.hopbyhopid[0], s6AuthDetails.endtoendid[0]]
                s6Fields.append(s6AuthDetails.rat_type[0] if len(s6AuthDetails.rat_type) else '')
                s6Fields.append(s6AuthDetails.imsi[0] if len(s6AuthDetails.imsi) else '')
                s6Fields.append(s6AuthDetails.imei[0] if len(s6AuthDetails.imei) else '')
                s6Fields.append(s6AuthDetails.msisdn[0] if len(s6AuthDetails.msisdn) else '')
                for xres in s6AuthDetails.xres:
                    all_xres += xres + ","
                s6Fields.append(all_xres[:-1])
                for kasme in s6AuthDetails.kasme:
                    all_kasme += kasme + ","
                s6Fields.append(all_kasme[:-1])

                self.sql.Insert_Or_Update("S6", s6Fields)

            except Exception as e:
                print e

    def get_counters(self):
        return self.num_of_messages

    def print_counters(self):
        logger.info(self.counters)

    def packable_get(self):
        tup = ("S6", self.counters, None)
        return tup

    def push_counters(self):
        serialized_data = self.mainCollector.pickle_pack(self.packable_get())
        self.collectorCarrier.put(serialized_data)
        self.reset_counters()

    def reset_counters(self, displayAll = False):
        self.counters.full_display = displayAll
        self.counters.AuthenticationInformationRequest = 0
        self.counters.AuthenticationInformationAnswer = 0
        self.counters.UpdateLocationRequest = 0
        self.counters.UpdateLocationAnswer = 0
        self.counters.InsertSubscriberDataRequest = 0
        self.counters.InsertSubscriberDataAnswer = 0
        self.counters.PurgeUERequest = 0
        self.counters.PurgeUEAnswer = 0
        self.counters.IdentityCheckRequest = 0
        self.counters.IdentityCheckAnswer = 0
        self.counters.CancelLocationRequest = 0
        self.counters.CancelLocationAnswer = 0
        self.counters.NotifyRequest = 0
        self.counters.NotifyAnswer = 0
        self.counters.DeviceWatchdogRequest = 0
        self.counters.DeviceWatchdogAnswer = 0
        self.counters.Others = 0
        self.counters.layer = 0

