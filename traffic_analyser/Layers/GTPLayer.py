import logging
import pickle
import string

from ExtractFields import *
from IpLayer import *
from Counters import GTPV2Counters

globalGTPV2Details = {}

logger = logging.getLogger('')

class GTPV2Details:
    def __init__(self):
        self.imsi = []
        self.imei = []
        self.msisdn = []
        self.fteid_ipv4 = []
        self.fteid_iface = []
        self.mmeip = []
        self.sgwip = []
        self.fteid1 = ''
        self.fteid2 = ''
        self.fteid_st = []
        self.seq_num = []
        self.teid = []


    def __str__(self):
        return ("imsi=%s imei=%s msisdn=%s fteid_ipv4=%s fteid_iface=%s mmeip=%s sgwip=%s pgwip=%s "
                " fteid_st=%s seq_num=%s"
                %(self.imsi, self.imei, self.msisdn,self.fteid_ipv4, self.fteid_iface, self.mmeip,
                  self.sgwip, self.fteid_st, self.seq_num))



class GTPV2Processor:
    def __init__(self, sqlOperation, mainCollector, collectorCarrier, mme_ip):
        self.num_of_layers = 0
        self.sql = sqlOperation
        self.mmeip = mme_ip
        self.sgwip = ""
        #TODO add counters
        self.counters = GTPV2Counters(False)
        self.mainCollector = mainCollector
        self.collectorCarrier = collectorCarrier
        self.cur_pkt_cnt = 0
        self.old_pkt_cnt = 0
        self.prev_fteid1 = ''
        self.prev_fteid2 = ''


    def insert_gtpv2_details(self, gtpv2Details, pkt_count):
        # FIXME: Currently working only with enbid, should bring upon mmeid logic too.
        if len(gtpv2Details.enbid) <= 0:
            return

        key = gtpv2Details.enbid[0]
        if not globalGTPV2Details.has_key(key):
            globalGTPV2Details[key] = gtpv2Details
        else:
            lhs = globalGTPV2Details[key]
            globalGTPV2Details[key] = lhs.merge(gtpv2Details)

        logger.debug ("pkt=%s %s" %(pkt_count, globalGTPV2Details[key]))


    def update_counter(self, value):
        gtpv2counters = self.counters
        gtpv2counters.layer +=1
        if 'CreateSessionRequest' == value:
            gtpv2counters.CreateSessionRequest +=1
        elif 'CreateSessionResponse' == value:
            gtpv2counters.CreateSessionResponse +=1
        elif 'ModifyBearerRequest' == value:
            gtpv2counters.ModifyBearerRequest += 1
        elif 'ModifyBearerResponse' == value:
            gtpv2counters.ModifyBearerResponse += 1
        elif 'DeleteSessionRequest' == value:
            gtpv2counters.DeleteSessionRequest += 1
        elif 'DeleteSessionResponse' == value:
            gtpv2counters.DeleteSessionResponse += 1
        elif 'CreateBearerRequest' == value:
            gtpv2counters.CreateBearerRequest += 1
        elif 'CreateBearerResponse' == value:
            gtpv2counters.CreateBearerResponse += 1
        elif 'UpdateBearerRequest' == value:
            gtpv2counters.UpdateBearerRequest += 1
        elif 'UpdateBearerResponse' == value:
            gtpv2counters.UpdateBearerResponse += 1
        elif 'DeleteBearerRequest' == value:
            gtpv2counters.DeleteBearerRequest += 1
        elif 'DeleteBearerResponse' == value:
            gtpv2counters.DeleteBearerResponse += 1
        elif 'ReleaseAccessBearerRequest' == value:
            gtpv2counters.ReleaseAccessBearerRequest += 1
        elif 'ReleaseAccessBearerResponse' == value:
            gtpv2counters.ReleaseAccessBearerResponse += 1
        elif 'ForwardRelocationRequest' == value:
            gtpv2counters.ForwardRelocationRequest += 1
        elif 'ForwardRelocationResponse' == value:
            gtpv2counters.ForwardRelocationResponse += 1
        elif 'ForwardRelocationCompleteNotification' == value:
            gtpv2counters.ForwardRelocationCompleteNotification += 1
        elif 'ForwardRelocationCompleteAcknowledge' == value:
            gtpv2counters.ForwardRelocationCompleteAcknowledge += 1
        elif 'ContextRequest' == value:
            gtpv2counters.ContextRequest += 1
        elif 'ContextResponse' == value:
            gtpv2counters.ContextResponse += 1
        elif 'ContextAcknowledge' == value:
            gtpv2counters.ContextAcknowledge += 1
        elif 'ForwardAccessCompleteNotification' == value:
            gtpv2counters.ForwardAccessCompleteNotification += 1
        elif 'ForwardAccessCompleteAcknowledge' == value:
            gtpv2counters.ForwardAccessCompleteAcknowledge += 1
        elif 'RelocationCancelRequest' == value:
            gtpv2counters.RelocationCancelRequest += 1
        elif 'RelocationCancelResponse' == value:
            gtpv2counters.RelocationCancelResponse += 1
        elif 'ReleaseAccessBearersRequest' == value:
            gtpv2counters.ReleaseAccessBearersRequest += 1
        elif 'ReleaseAccessBearersResponse' == value:
            gtpv2counters.ReleaseAccessBearersResponse += 1
        elif 'DownlinkDataNotification' == value:
            gtpv2counters.DownlinkDataNotification += 1
        elif 'DownlinkDataNotificationAcknowledgement' == value:
            gtpv2counters.DownlinkDataNotificationAcknowledgement += 1
        elif 'DownlinkDataNotificationFailureIndication' == value:
            gtpv2counters.DownlinkDataNotificationFailureIndication += 1
        else:
            gtpv2counters.Others +=1




    def process_gtpv2_layer(self, pkt_count, pkt_time, src, dst, fields, fname):
        self.num_of_layers += 1
        self.cur_pkt_cnt = pkt_count
        if self.old_pkt_cnt > self.cur_pkt_cnt:
            self.old_pkt_cnt = 0  # Time to reset it.

        if len(fields):
            gtpv2Details = GTPV2Details()
            cmd = ''
            for key,value in fields.items():
                if len(value) > 0:
                    if 'Sequence Number' in key:
                        gtpv2Details.seq_num.append(value[0][0])
                    elif '(IMSI)' in key:
                        gtpv2Details.imsi.append(value[0][0])
                    elif 'MEI(Mobile Equipment Identity)' == key:
                        gtpv2Details.imei.append(value[0][0])
                    elif 'MSISDN' in key:
                        gtpv2Details.msisdn.append(value[0][0])
                    elif 'Tunnel Endpoint Identifier' == key:
                        gtpv2Details.teid.append(value[0][0])
                    elif 'Fully Qualified Tunnel Endpoint Identifier (F-TEID)' in key:
                        for i in range(len(value)):
                            if ((value[i][1] == 'MME') and (value[i][2] == 'GTP-C')) :
                                gtpv2Details.fteid1 = str(int((value[i][-3]), 16))
                            if ((value[i][1] == 'SGW') and (value[i][2] == 'GTP-C')) :
                                self.sgwip = value[i][-1]
                                gtpv2Details.fteid2 = str(int((value[i][-3]), 16))
#                            logger.info("fteid1 %s and fteid2 %s", gtpv2Details.fteid1,gtpv2Details.fteid2)
                    elif 'Message Type' == key:
                        cmd = ''.join(str(x) for x in value[0][:-1])
                        self.update_counter(cmd)


            #FIXME: This is dirty filling, may be try filling it via indices
            gtpv2dbfields=[]
            gtpv2dbfields.append((gtpv2Details.seq_num[0]) if len(gtpv2Details.seq_num) else '')
            gtpv2dbfields.append((gtpv2Details.teid[0]) if len(gtpv2Details.teid) else '')
            gtpv2dbfields.append((gtpv2Details.imsi[0]) if len (gtpv2Details.imsi) else '')
            gtpv2dbfields.append((gtpv2Details.imei[0]) if len(gtpv2Details.imei) else '')
            gtpv2dbfields.append((gtpv2Details.msisdn[0]) if len(gtpv2Details.msisdn) else '')
            if(len(gtpv2Details.fteid1)) :
                gtpv2dbfields.append(gtpv2Details.fteid1)
            else :
                gtpv2dbfields.append(self.prev_fteid1)
            if(len(gtpv2Details.fteid2)) :
                gtpv2dbfields.append(gtpv2Details.fteid2)
            else :
                gtpv2dbfields.append(self.prev_fteid2)
            if(len(gtpv2Details.fteid1)) :
                self.prev_fteid1 = gtpv2Details.fteid1
            if(len(gtpv2Details.fteid2)) :
                self.prev_fteid2 = gtpv2Details.fteid2

            try:
                direction = self.get_msg_direction[cmd]
            except:
                logger.debug("Message %s is not important " % cmd)
                direction = -1

            callFlowdb = []
            callFlowdb.append(pkt_count)
            callFlowdb.append(pkt_time)
            callFlowdb.append(fname)
            callFlowdb.append(gtpv2dbfields[0])  # seq_num
            callFlowdb.append(gtpv2dbfields[1]) #teid
            if direction:
                callFlowdb.append(src)
                callFlowdb.append(dst)
            else:
                callFlowdb.append(dst)
                callFlowdb.append(src)

            if self.mmeip is not None:
                if callFlowdb[5] not in self.mmeip:
                    return

            callFlowdb.append(direction)
            callFlowdb.append(gtpv2dbfields[5]) #fteid1
            callFlowdb.append(gtpv2dbfields[6]) #fteid2
            callFlowdb.append(cmd)  # message

            if direction != -1:
                try:
                    self.sql.Insert_Or_Update("GTPC", gtpv2dbfields)
                    self.sql.Insert("CALLFLOW_GTPC", callFlowdb)

                except Exception as e:
                    print (e)


    get_msg_direction = {
            'CreateSessionRequest': 1,
            'CreateSessionResponse' : 0,
            'ModifyBearerRequest' : 1,
            'ModifyBearerResponse' : 0,
            'DeleteSessionRequest' : 1,
            'DeleteSessionResponse' : 0,
            'CreateBearerRequest' : 0,
            'CreateBearerResponse' : 1,
            'UpdateBearerRequest' : 0,
            'UpdateBearerResponse' : 1,
            'DeleteBearerRequest' : 0,
            'DeleteBearerResponse' : 1,
            'ReleaseAccessBearerRequest' : 1,
            'ReleaseAccessBearerResponse' : 0,
            'ReleaseAccessBearersRequest' : 1,
            'ReleaseAccessBearersResponse' : 1,
            'DownlinkDataNotification' : 0,
            'DownlinkDataNotificationAcknowledgement' : 1,
            'DownlinkDataNotificationFailureIndication' : 1,
            'ForwardRelocationRequest' : 1,
            'ForwardRelocationResponse' : 0,
            'ForwardRelocationCompleteNotification' : 1,
            'ForwardRelocationCompleteAcknowledge' : 0,
            'ContextRequest' : 1,
            'ContextResponse' : 0,
            'ContextAcknowledge' : 0,
            'ForwardAccessCompleteNotification' : 1,
            'ForwardAccessCompleteAcknowledge' : 0,
            'RelocationCancelRequest' : 1,
            'RelocationCancelResponse' : 0,
    }


    def get_counters(self):
        return self.num_of_layers

    def print_counters(self):
        logger.info(self.counters)


    def packable_get(self):
        tup = ("DATA", self.counters, None)
        return tup

    def push_counters(self):
        serialized_data = self.mainCollector.pickle_pack(self.packable_get())
        self.collectorCarrier.put(serialized_data)
        self.reset_counters()

    def reset_counters(self, displayAll = False):
        self.counters.full_display = displayAll
        self.counters.CreateSessionRequest = 0
        self.counters.CreateSessionResponse = 0
        self.counters.ModifyBearerRequest = 0
        self.counters.ModifyBearerResponse = 0
        self.counters.DeleteSessionRequest = 0
        self.counters.DeleteSessionResponse = 0
        self.counters.CreateBearerRequest = 0
        self.counters.CreateBearerResponse = 0
        self.counters.UpdateBearerRequest = 0
        self.counters.UpdateBearerResponse = 0
        self.counters.DeleteBearerRequest = 0
        self.counters.DeleteBearerResponse = 0
        self.counters.ReleaseAccessBearerRequest = 0
        self.counters.ReleaseAccessBearerResponse = 0
        self.counters.ForwardRelocationRequest = 0
        self.counters.ForwardRelocationResponse = 0
        self.counters.ForwardRelocationCompleteNotification = 0
        self.counters.ForwardRelocationCompleteAcknowledge = 0
        self.counters.ContextRequest = 0
        self.counters.ContextResponse = 0
        self.counters.ContextAcknowledge = 0
        self.counters.ForwardAccessCompleteNotification = 0
        self.counters.ForwardAccessCompleteAcknowledge = 0
        self.counters.RelocationCancelRequest = 0
        self.counters.RelocationCancelResponse = 0
        self.counters.ReleaseAccessBearersRequest = 0
        self.counters.ReleaseAccessBearersResponse = 0
        self.counters.DownlinkDataNotification = 0
        self.counters.DownlinkDataNotificationAcknowledgement = 0
        self.counters.DownlinkDataNotificationFailureIndication = 0
        self.counters.Others = 0
        self.counters.layer = 0

