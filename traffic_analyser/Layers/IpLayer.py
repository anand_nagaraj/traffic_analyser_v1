from pyshark.packet.layer import *

def ip_layer(packet):
    ip = packet['ip']

    if (ip != None and isinstance(ip, Layer)):
        print('IP Layer:')
        # print('This is Layer instance')
        id = ip.get_field('id')
        print ('Identification: %s %s' % (id, hex(int(id))))


def extract_src_dst_ip_address(packet):
    ip = packet.ip
    return [ip.src, ip.dst]