from pyshark.packet.layer import *

class Dictlist(dict):
    def __setitem__(self, key, value):
        try:
            self[key]
        except KeyError:
            super(Dictlist, self).__setitem__(key, [])
        self[key].append(value)

def frame_level(packet, count):
    return ("pkt=%s %s" % (count, packet.sniff_time))


def get_pkt_time(packet):
    return ("%s" % packet.sniff_time)

def extractAllFields(layer, extended_fields = False):
    field_pairs = {}
    #list_val = []
    for field_line in layer._get_all_field_lines():
        if ':' in field_line:
            key, value = field_line.split(':', 1)
            key = key.split('\t')[1]
            if not extended_fields:
                value = value.split(' ')[1].split('\n')[0]
            else:
                value = value.split()

            if field_pairs.has_key(key):
                #list_val = list(field_pairs[key])
                #del list_val[:]
                # list_val = []
                list_val = field_pairs[key]
                if value not in list_val:
                    list_val.append(value)

            else:
                list_val = []
                list_val.append(value)

            field_pairs[key] = list_val

    return field_pairs


def isExtractable(layer):
    if len(layer._all_fields) == 0 :
        return False

    return True

def isFieldExtractable(layer, field):
    return layer.get(field)

def convertToBCD(value):
    return value
    # FIXME: need to work on this, convert to bcd.
    # bcd_value = " ".join(["0" * (4 - len(bin(int(number,16))[2:])) + bin(int(number,16))[2:] for number in value])
    # return bcd_value
