import pickle
import logging
import time
from Layers.Counters import MainCollectorCounters
from MultiProcessing import Process, Queue

logger = logging.getLogger('')

def human_readable(diff):
    hours = int((diff // 3600) % 24)
    minutes = int((diff // 60) % 60)
    seconds = diff % 60
    return ("%s:%s:%2.6s" %(hours, minutes, seconds))


class CollectorWorkerClass:
    def __init__(self, cntlock):
        self.mainCollectorCounter = MainCollectorCounters(cntlock)
        pass

    def __str__(self):
        return self.print_counters()

    def pickle_pack(self, msg):
        return pickle.dumps(msg)

    def pickle_unpack(self, msg):
        return pickle.loads(msg)

    def PrepareCollectorWorker(self):
        namevar = "Collector"
        carrier = Queue()
        worker = Process(name=namevar, target=self.StartCollectorWorker,
                         args=(carrier,))
        worker.start()
        return worker, carrier


    def StartCollectorWorker(self, carrier):
        startTime = time.time()
        prev = startTime
        while True:
            try:
                msg = carrier.get()  # Read it from the queue
                if msg is not None:
                    type, counters, pkt_cnt = self.pickle_unpack(msg)
                    if type == 'S1':
                        self.mainCollectorCounter.merge_s1(counters)
                    elif type == 'S6':
                        self.mainCollectorCounter.merge_s6(counters)
                    elif type == 'DATA':
                        self.mainCollectorCounter.merge_gtpv2(counters)
                    elif type == 'TA':
                        self.mainCollectorCounter.merge_pkt_cnt(pkt_cnt)
                    elif type == 'EOF':
                        self.print_counters(startTime, True)
                        raise EOFError

                cur = time.time()
                if cur > prev + 10:
                    self.print_counters(startTime)
                    prev = cur

            except EOFError:
                logger.info("Collector worker received its last message.")
                break
            except Exception as e:
                logger.exception("Exception raised due to %s" % e)

    def PushStats(self, serialized_msg):
        pass

    def print_counters(self, startTime, eof=False):
        cur = time.time()
        diff = int(cur - startTime)
        if eof:
            preamble = ('\n\n-----------FINAL STATS running since %s ----------'
                        % (human_readable(diff)))
        else:
            preamble = ('------since %s --------' % (human_readable(diff)))
        logger.info("%s %s" % (preamble, self.mainCollectorCounter))


