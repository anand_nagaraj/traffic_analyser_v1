#!/bin/bash

# Get the hash for the current commit, and append -dirty if there are
# locally uncommited changes
hash()
{
  # get the short revision specifier
    set -- $(git rev-parse --short ${HASH_SPEC})
    HASH=$1

    RET_HASH=${HASH}${DIRTY}
}


# Return the ident based on hash or on top of git stack
ident()
{
    hash
    DESCR=$(git describe --tags)
    release_date
    echo "${RELEASE_DATE} master@${RET_HASH}  ${DESCR}"
}


#
# Automatically determine what the release date for this build should
# be. This is taken from the commit date (not the author date - we
# want the date it was commited to git, not the date that the code
# was originally written) and convert it to the UTC time in the
# required format for the release date.
#
release_date()
{
    UNIX_DATE=$(git log -n 1 --pretty="%ct" ${HASH_SPEC})
    RELEASE_DATE=$(date -d@${UNIX_DATE} -u +"%Y/%m/%d")
}

usage()
{
    echo $"Usage: $0 {ident|hash|releasedate|help}"
    exit 1;
}


#---------------------------------------------------------------------------------------------------------
#---------------------------------------Start of main-----------------------------------------------------
#---------------------------------------------------------------------------------------------------------

# Find root of the git tree
GIT_ROOT=$(git rev-parse --show-toplevel)
if [ -z "${GIT_ROOT}" ]; then
   # Not in a git repository - the git command has already logged this
    exit 1
fi

TAG_REL_1="dev_trafficanalyser_0.0.0.2"
UNIX_DATE_RELEASE_V1=$(git log -n 1 --pretty="%ct" ${TAG_REL_1})
TAG_HASH_REL_1=$(git log --oneline -n1 $TAG_REL_1 | cut -f1 -d ' ')
HASH_SPEC=HEAD


case "$1" in
    hash)
        hash
        echo $RET_HASH
        ;;
    ident)
        ident
        ;;
    releasedate)
        release_date
        echo $RELEASE_DATE
        ;;
    help)
        usage
        ;;
    *)
        usage
        ;;
esac

exit 0
